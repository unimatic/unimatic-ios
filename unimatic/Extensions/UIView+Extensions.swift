//
//  UIView+Additions.swift
//  Buddyfit Production
//
//  Created by Andrea Armiliato on 20/11/2019.
//  Copyright © 2019 Tripix Tech Srl. All rights reserved.
//

import UIKit

extension UIView {
  
  class func fromNib<T: UIView>() -> T {
    return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
  }
  
  func findViewController() -> UIViewController? {
    if let nextResponder = self.next as? UIViewController {
      return nextResponder
    } else if let nextResponder = self.next as? UIView {
      return nextResponder.findViewController()
    } else {
      return nil
    }
  }
  
  var size: CGSize {
    get {
      return self.frame.size
    } set {
      self.frame = CGRect(x: self.frame.minX, y: self.frame.minY, width: newValue.width, height: newValue.height)
    }
  }
  
  var width: CGFloat {
    get {
      return self.frame.width
    } set {
      self.frame = CGRect(x: self.frame.minX, y: self.frame.minY, width: newValue, height: self.frame.height)
    }
  }
  
  var height: CGFloat {
    get {
      return self.frame.height
    } set {
      self.frame = CGRect(x: self.frame.minX, y: self.frame.minY, width: self.frame.width, height: newValue)
    }
  }
  
  var x: CGFloat {
    get {
      return self.frame.minX
    } set {
      self.frame = CGRect(x: newValue, y: self.frame.minY, width: self.frame.width, height: self.frame.height)
    }
  }
  
  var y: CGFloat {
    get {
      return self.frame.minY
    } set {
      self.frame = CGRect(x: self.frame.minX, y: newValue, width: self.frame.width, height: self.frame.height)
    }
  }
  
  var maxX: CGFloat {
    get {
      return self.frame.maxX
    } set {
      self.frame = CGRect(x: newValue - self.width, y: self.y, width: self.width, height: self.height)
    }
  }
  
  var maxY: CGFloat {
    get {
      return self.frame.maxY
    } set {
      self.frame = CGRect(x: self.x, y: newValue - self.height, width: self.width, height: self.height)
    }
  }
  
  var midX: CGFloat {
    get {
      return self.frame.midX
    } set {
      self.frame = CGRect(x: newValue - self.width / 2, y: self.y, width: self.width, height: self.height)
    }
  }
  
  var midY: CGFloat {
    get {
      return self.frame.midY
    } set {
      self.frame = CGRect(x: self.x, y: newValue - self.height / 2, width: self.width, height: self.height)
     }
  }
  
  func removeAllSubviews() {
    
    self.subviews.forEach { subview in
      subview.removeFromSuperview()
    }
  }

}
