//
//  UIBezierPath+Extensions.swift
//  unimatic
//
//  Created by Filippo Tosetto on 03/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

extension UIBezierPath {
  
  convenience init(circleSegmentCenter center:CGPoint, radius:CGFloat, startAngle:CGFloat, endAngle:CGFloat) {
      self.init()
    self.move(to: CGPoint(x: center.x, y: center.y))
    self.addArc(withCenter: center, radius:radius, startAngle:startAngle.radians(), endAngle: endAngle.radians(), clockwise:true)
    self.close()
  }
}
