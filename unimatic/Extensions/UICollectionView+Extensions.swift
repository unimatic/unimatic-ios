//
//  UICollectionView+Extensions.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 14/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import UIKit

extension UICollectionView {
  
  func registerNibClass<T: UICollectionViewCell>(_ nibClass: T.Type) {
    self.register(UINib(nibName: String(describing: nibClass), bundle: nil), forCellWithReuseIdentifier: String(describing: nibClass))
  }
  
  func dequeReusableCellWithClass<T: UICollectionViewCell>(_ cellClass: T.Type, forIndexPath: IndexPath) -> T? {
    return self.dequeueReusableCell(withReuseIdentifier: String(describing: cellClass), for: forIndexPath) as? T
  }
  
  func registerFooterNibClass<T: UICollectionReusableView>(_ nibClass: T.Type) {
    self.register(UINib(nibName: String(describing: nibClass), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: String(describing: nibClass))
  }
  
  func dequeReusableFooterWithClass<T: UICollectionReusableView>(_ footerClass: T.Type, forIndexPath: IndexPath) -> T? {
    return self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: String(describing: footerClass), for: forIndexPath) as? T
  }
  
  func registerHeaderNibClass<T: UICollectionReusableView>(_ nibClass: T.Type) {
    self.register(UINib(nibName: String(describing: nibClass), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: nibClass))
  }
  
  func dequeReusableHeaderWithClass<T: UICollectionReusableView>(_ headerClass: T.Type, forIndexPath: IndexPath) -> T? {
    return self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: headerClass), for: forIndexPath) as? T
  }
  
  func scrollToBottom() {
    let collectionViewContentHeight = self.contentSize.height;
    let collectionViewFrameHeightAfterInserts = self.height - (self.contentInset.top + self.contentInset.bottom);
    
    if(collectionViewContentHeight > collectionViewFrameHeightAfterInserts) {
      self.contentOffset = CGPoint(x: 0, y: self.contentSize.height - self.frame.size.height)
    }
  }
  
  func performBatchUpdates(animated: Bool, updates: (() -> Swift.Void)?, completion: ((Bool) -> Swift.Void)? = nil) {
    
    if !animated {
      UIView.animate(withDuration: 0, animations: {
        self.performBatchUpdates(updates, completion: completion)
      })
      
    } else {
      self.performBatchUpdates(updates, completion: completion)
    }
  }
  
  func convertAttributesForItem(at: IndexPath, to coordinateSpace: UICoordinateSpace) -> CGRect {
    
    let layoutAttributes = self.layoutAttributesForItem(at: at)
    let attributesFrame = layoutAttributes?.frame
    return self.convert(attributesFrame!, to: coordinateSpace)
  }
}
