//
//  String+Extensions.swift
//  unimatic
//
//  Created by Filippo Tosetto on 30/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

extension String {
  
  static func unique() -> String {
    return ProcessInfo.processInfo.globallyUniqueString
  }

  var localized: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
  }
  
  var lastPathComponent: String {
    return (self as NSString).lastPathComponent
  }
  
  var deletePathExtension: String {
    return (self as NSString).deletingPathExtension
  }
}
