//
//  UIViewController+Extensions.swift
//  unimatic
//
//  Created by Filippo Tosetto on 04/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

extension UIViewController {
  
  func isVisible() -> Bool {
    return self.isViewLoaded && self.view.window != nil
  }
}
