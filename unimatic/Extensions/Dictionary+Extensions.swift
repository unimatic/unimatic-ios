//
//  Dictionary+Extensions.swift
//  unimatic
//
//  Created by Filippo Tosetto on 03/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

extension Dictionary {
  
  var jsonStringRepresentation: String? {
    guard let theJSONData = try? JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted]) else {
      return nil
    }

    return String(data: theJSONData, encoding: .ascii)
  }
  
}

extension NSDictionary {
  
  func stringForCaseInsensitiveKey(_ key: AnyObject?) -> String? {
   
    var searchedValue: String? = nil

    if let insensitiveKey = key as? String {
      
      self.forEach { (key: Any, value: Any) in
        if let key = (key as? String), key.lowercased() == insensitiveKey.lowercased() {
          searchedValue = value as? String
          return
        }
      }
    }
    
    return searchedValue
  }
}
