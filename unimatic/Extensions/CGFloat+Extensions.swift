//
//  CGFloat+Extensions.swift
//  unimatic
//
//  Created by Filippo Tosetto on 03/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import CoreGraphics

extension CGFloat {
    
  func degrees() -> CGFloat {
    return 180 * (self/CGFloat.pi)
  }
  
  func radians() -> CGFloat {
    return CGFloat.pi * (self/180)
  }
}
