//
//  Date+Extensions.swift
//  unimatic
//
//  Created by Filippo Tosetto on 29/04/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

extension Date {
  
  static var gregorianCalendar: Calendar {
    struct Static {
      
      static let instance: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    }
    
    return Static.instance
  }
  
  func dateByAddingHours(_ hours: Int) -> Date? {
    
    var dateComponents = DateComponents()
    dateComponents.hour = hours
    return (Date.gregorianCalendar as NSCalendar).date(byAdding: dateComponents, to: self, options: [])
  }
  
  func dateByAddingHinutes(_ minutes: Int) -> Date? {
    
    var dateComponents = DateComponents()
    dateComponents.minute = minutes
    return (Date.gregorianCalendar as NSCalendar).date(byAdding: dateComponents, to: self, options: [])
  }
  
  func dateByAddingDays(_ days: Int) -> Date? {
    
    var dateComponents = DateComponents()
    dateComponents.day = days
    return (Date.gregorianCalendar as NSCalendar).date(byAdding: dateComponents, to: self, options: [])
  }
  
  func getDays() -> Int {
    return (Date.gregorianCalendar as NSCalendar).component(.day, from: self)
  }

  func getHours() -> Int {
    return (Date.gregorianCalendar as NSCalendar).component(.hour, from: self)
  }

  func getMinutes() -> Int {
    return (Date.gregorianCalendar as NSCalendar).component(.minute, from: self)
  }

  /// Returns the amount of years from another date
  func years(from date: Date) -> Int {
    return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
  }
  /// Returns the amount of months from another date
  func months(from date: Date) -> Int {
    return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
  }
  /// Returns the amount of weeks from another date
  func weeks(from date: Date) -> Int {
    return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
  }
  /// Returns the amount of days from another date
  func days(from date: Date) -> Int {
    return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
  }
  /// Returns the amount of hours from another date
  func hours(from date: Date) -> Int {
    return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
  }
  /// Returns the amount of minutes from another date
  func minutes(from date: Date) -> Int {
    return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
  }
  /// Returns the amount of seconds from another date
  func seconds(from date: Date) -> Int {
    return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
  }
  /// Returns the amount of nanoseconds from another date
  func nanoseconds(from date: Date) -> Int {
    return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
  }
  /// Returns the a custom time interval description from another date
  func offset(from date: Date) -> String {
    let formatter = DateComponentsFormatter()
    formatter.unitsStyle = .brief // May delete the word brief to let Xcode show you the other options
    formatter.allowedUnits = [.day, .hour, .minute]
    formatter.maximumUnitCount = 2 

    return formatter.string(from: date, to: self) ?? ""
  }
}
