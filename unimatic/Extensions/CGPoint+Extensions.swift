//
//  CGPoint+Extensions.swift
//  unimatic
//
//  Created by Filippo Tosetto on 03/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import CoreGraphics

extension CGPoint {
  
   static func angleBetweenLinesInDegrees(beginLineA: CGPoint, endLineA: CGPoint, beginLineB: CGPoint, endLineB: CGPoint) -> CGFloat {
     let a = endLineA.x - beginLineA.x
     let b = endLineA.y - beginLineA.y
     let c = endLineB.x - beginLineB.x
     let d = endLineB.y - beginLineB.y
  
     let atanA = atan2(a, b)
     let atanB = atan2(c, d)
  
       // convert radiants to degrees
     return (atanA - atanB) * 180 / CGFloat.pi
   }
   
   static func distanceBetweenPoints(point1: CGPoint, point2: CGPoint) -> CGFloat {
     let dx = point1.x - point2.x
     let dy = point1.y - point2.y
     return sqrt(dx*dx + dy*dy)
   }
}
