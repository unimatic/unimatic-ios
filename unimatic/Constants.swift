//
//  Constants.swift
//  unimatic
//
//  Created by Filippo Tosetto on 29/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

struct Constants {
  
  struct Colors {
    static let primary = UIColor(red: 24.0/255.0, green: 226.0/255.0, blue: 0.0, alpha: 1.0)
    static let secondary = UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 228.0/255.0, alpha: 1.0)
  }  
}
