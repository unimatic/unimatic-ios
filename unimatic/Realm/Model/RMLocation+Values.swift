//
//  RMLocation+Values.swift
//  unimatic
//
//  Created by Filippo Tosetto on 03/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import CoreLocation

extension RMLocation {
  
  var coordinates: CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
  }
  
  var location: CLLocation {
    return CLLocation(latitude: self.latitude, longitude: self.longitude)
  }
  
  var timezone: TimeZone? {
    return TimeZone(identifier: self.timeZoneIdentifier ?? "")
  }
  
  func getSolar(forDate date: Date) -> (sunrise: Date?, sunset: Date?) {
    let solar = Solar(for: date, coordinate: self.coordinates)
    return (sunrise: solar?.sunrise, sunset: solar?.sunset)
  }
}
