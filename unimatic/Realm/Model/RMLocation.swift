//
//  RMLocation.swift
//  unimatic
//
//  Created by Filippo Tosetto on 03/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Realm
import RealmSwift
import CoreLocation

class RMLocation: Object {
  @objc dynamic var latitude: Double = 0.0
  @objc dynamic var longitude: Double = 0.0
  @objc dynamic var name: String = ""
  @objc dynamic var timeZoneIdentifier: String? = nil
  @objc dynamic var current: Bool = false
  
  init(placemark: CLPlacemark) {
    self.latitude = placemark.location?.coordinate.latitude ?? 0.0
    self.longitude = placemark.location?.coordinate.latitude ?? 0.0
    self.name = placemark.locality ?? placemark.name ?? ""
    self.timeZoneIdentifier = placemark.timeZone?.identifier
    self.current = false
  }
  
  init(title: String, center: CLLocationCoordinate2D, timeZone: TimeZone?) {
    self.name = title
    self.latitude = center.latitude
    self.longitude = center.longitude
    self.timeZoneIdentifier = timeZone?.identifier
    self.current = false
  }
  
  required init() {
    self.latitude = 0.0
    self.longitude = 0.0
    self.name = ""
    self.timeZoneIdentifier = nil
    self.current = false
  }
  
  public override func isEqual(_ object: Any?) -> Bool {
    return self.name == (object as? RMLocation)?.name &&
      self.latitude == (object as? RMLocation)?.latitude &&
      self.longitude == (object as? RMLocation)?.longitude
  }
}
