//
//  RMQuery.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 13/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import RealmSwift

// MARK: - RMQueryChanges

struct RMQueryChanges {
  
  var inserts = [Int]()
  var deletes = [Int]()
  var updates = [Int]()
  
  var reload = false
}

// MARK: - RMQueryDelegate

protocol RMQueryDelegate: class {
  
  func realmQueryDidChange(_ changes: RMQueryChanges, query: AnyObject)
  func realmQueryDidInvalidate(query: AnyObject)
}

extension RMQueryDelegate {
  
  // make delegates optional
  func realmQueryDidChange(_ changes: RMQueryChanges, query: AnyObject) {}
  func realmQueryDidInvalidate(query: AnyObject) {}
}

// MARK: - RMQuery

class RMQuery<T: Object> {
  
  fileprivate var token: NotificationToken?
  
  fileprivate weak var delegate: RMQueryDelegate? {
    didSet {
      guard let _ = self.delegate else { return }
      self.token = self.results.observe { [weak self] (changes: RealmCollectionChange) in
        self?.notifyChanges(changes)
      }
    }
  }
  
  fileprivate let queue = DispatchQueue(label: "")
  
  // Available only for Single configuration
  fileprivate var cached_count: Int? = nil
  
  lazy var id = String.unique()
  
  var results: Results<T>!
  
  var fetch: (() -> Results<T>)! {
    didSet {
      self.results = fetch()
    }
  }
  
  var getCache: ((T) -> [Int])? = nil
  var cachedProperty = [[Int]]()
  
  // Realm seems to have problems with managing efficiently count and isEmpty
  // this one is not safe, could crash if one query delegate triggers another query count
  // TODO: remove once Realm fixes that - https://github.com/realm/realm-cocoa/issues/2622
  // values can be cached only if we have delegate (notifications) available
  var count: Int {
    if let count = self.cached_count, self.delegate != nil {
      return count
    } else {
      self.cached_count = self.results.count
      return self.cached_count!
    }
  }
  
  var isEmpty: Bool {
    return self.count == 0
  }
  
  init() {
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: RealmManager.resetNotification), object: nil, queue: nil) { [weak self] _ in
      
      if let this = self {
        self?.cached_count = nil
        self?.delegate?.realmQueryDidInvalidate(query: this)
      }
      
      self?.delegate = nil
    }
  }
  
  deinit {
    self.token?.invalidate()
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Working with result
  
  func itemForIndex(_ index: Int) -> T? {
    // Ensuring the the user is requesting for the right index, otherwise we can get a good crash.
    guard index < self.results.count else {
      return nil
    }
    
    return self.results[index]
  }
  
  // MARK: - RMQuery
  
  func itemForIndexPath(_ indexPath: IndexPath) -> T? {
    
    guard self.results.count > indexPath.row else { return nil }
    return self.results[indexPath.row]
  }
  
  // MARK: - Setup / Updates
  
  func setDelegate(delegate: RMQueryDelegate?) {
    self.delegate = delegate
  }
  
  func refreshCount() {
    
    // if we have 2 equal queries, then they might end up having
    // different counts, if notification for first one was send
    // and second one will receive notification only on next run loop
    // in this case you can use refreshCount to force loading count from query results
    // rather than cache
    // NB! if you use tableView / collectionView that handles inserts / deletes / moves
    // calling refreshCount before getting actual changes delegate might cause data source exception
    
    self.cached_count = nil
  }
  
  fileprivate func notifyChanges(_ changes: RealmCollectionChange<Results<T>>) {
    
    var queryChanges = RMQueryChanges()
    
    switch changes {
    case .initial:
      queryChanges.reload = true
      self.cached_count = self.results.count
      
      if let getCache = self.getCache {
        self.cachedProperty = self.results.map { getCache($0) }
      }
      
      self.delegate?.realmQueryDidChange(queryChanges, query: self)
      
      break
      
    case .update(_, let deletions, let insertions, let modifications):
      
      self.cached_count = self.results.count
      
      queryChanges.inserts = insertions
      queryChanges.deletes = deletions
      queryChanges.updates = modifications
      
      if let getCache = self.getCache {
        if !insertions.isEmpty || !deletions.isEmpty {
          self.cachedProperty = self.results.map { getCache($0) }
          
        } else if !modifications.isEmpty {
          for index in modifications {
            self.cachedProperty[index] = getCache(self.results[index])
          }
        }
      }
      
      self.delegate?.realmQueryDidChange(queryChanges, query: self)
      
      break
      
    case .error(let error):
      // An error occurred while opening the Realm file on the background worker thread
      IAssertLog("\(error)")
      break
    }
  }
}

// MARK: - RMQuery Equatable

extension RMQuery: Equatable { }

func ==<T>(lhs: RMQuery<T>, rhs: RMQuery<T>) -> Bool {
  return lhs.id == rhs.id
}
