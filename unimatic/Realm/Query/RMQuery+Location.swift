//
//  RMQuery+Location.swift
//  unimatic
//
//  Created by Filippo Tosetto on 04/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import RealmSwift

extension RMQuery {
  
  class func locationsQuery(delegate: RMQueryDelegate) -> RMQuery<RMLocation> {
    
    let query = RMQuery<RMLocation>()

    query.fetch = { (try! Realm()).objects(RMLocation.self).sorted(byKeyPath: "current", ascending: false) }
    query.setDelegate(delegate: delegate)
    
    return query
  }

  
  class func currentLocationQuery(delegate: RMQueryDelegate) -> RMQuery<RMLocation> {
    
    let query = RMQuery<RMLocation>()
    let predicate = NSPredicate(format: "current = true")

    query.fetch = { (try! Realm()).objects(RMLocation.self).filter(predicate) }
    query.setDelegate(delegate: delegate)
    
    return query
  }

}
