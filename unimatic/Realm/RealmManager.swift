//
//  RealmManager.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 13/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation
import FirebaseCrashlytics
import RealmSwift

// MARK: - SettingsRealm

func SettingsRealm() throws -> Realm {
  return try Realm(configuration: RealmManager.shared.settingsConfiguration)
}

// MARK: - RealmManager

class RealmManager {
  
  static let shared = RealmManager()

  static let resetNotification = "RealmManagerResetNotification"
  
  fileprivate let schemaVersion = (Bundle.main.infoDictionary!["RealmSchemaVersion"] as! NSNumber).uint64Value
  
  fileprivate let defaultURL: URL = {
    return Realm.Configuration.defaultConfiguration.fileURL!
  }()
    
  fileprivate let settingsURL: URL = {
    
    let documentsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    return documentsURL.appendingPathComponent("settings-db.realm")
  }()
  
  fileprivate var settingsConfiguration: Realm.Configuration!

  // MARK: - Setup
  
  func reset() {
    
    RMLeaveBreadcrumb()
    
    NotificationCenter.default.post(name: Notification.Name(rawValue: RealmManager.resetNotification), object: nil)
    
    self.testRealm(failedBlock: {
      self.hardReset(for: self.defaultURL)
      self.hardReset(for: self.settingsURL)
      
    }, passedBlock: {
      Realm.deleteAllRealms()
    })
  }
  
  func setup() {
    
    RMLeaveBreadcrumb(String(format: "core version: %d, url: %@", self.schemaVersion, self.defaultURL.absoluteString))
    
    print(self.schemaVersion)
    print(self.defaultURL)
    
    self.reloadConfigurations()
      
    self.testRealm(failedBlock: {
      self.hardReset(for: self.defaultURL)
      self.hardReset(for: self.settingsURL)
    })
  }
  
  func reloadConfigurations() {
    Realm.Configuration.defaultConfiguration = self.createDefaultConfiguration()
    self.settingsConfiguration = self.createSettingsConfiguration()
  }
  
  // MARK: - Testing migration
  
  fileprivate func hardReset(for realmURL: URL) {
    RMLeaveBreadcrumb()
    
    // use it only if db is corrupted and Realm crashes
    // for other cases user deleteAll() and deleteRealmIfMigrationNeeded
    
    do {
      let realmURLs = [
        realmURL,
        realmURL.appendingPathExtension("lock"),
        realmURL.appendingPathExtension("note"),
        realmURL.appendingPathExtension("management")
      ]
      
      for URL in realmURLs {
        RMLeaveBreadcrumb("\(URL)")
        
        do {
          try FileManager.default.removeItem(at: URL)
        } catch let error as NSError {
          RMLeaveBreadcrumb("\(error))")
        }
      }
    }
  }
  
  fileprivate func testRealm(failedBlock failed: () -> Void, passedBlock passed: (() -> Void)? = nil) {
    
    RMLeaveBreadcrumb()
    
    do {
      let _ = try Realm()
      let _ = try SettingsRealm()
      passed?()
      
    } catch let error as NSError {
      RMLeaveBreadcrumb(error.description)
      failed()
    }
  }
  
  // MARK: - Configurations
  
  fileprivate func createDefaultConfiguration() -> Realm.Configuration {
    
    let configuration = { () -> Realm.Configuration in
      
      #if FASTLANE
      return Realm.Configuration(inMemoryIdentifier: String.unique(), deleteRealmIfMigrationNeeded: true)
      
      #else
      if let _ = NSClassFromString("XCTest") {
        return Realm.Configuration(fileURL: self.defaultURL, inMemoryIdentifier: "BuddyfitDefaultRealm", deleteRealmIfMigrationNeeded: true)
      } else {
        return Realm.Configuration(fileURL: self.defaultURL, schemaVersion: self.schemaVersion, deleteRealmIfMigrationNeeded: true)
      }
      #endif
    }()
    
    return configuration
  }
    
  fileprivate func createSettingsConfiguration() -> Realm.Configuration {
    
    let configuration = { () -> Realm.Configuration in
      
      #if FASTLANE
      return Realm.Configuration(inMemoryIdentifier: String.unique(), deleteRealmIfMigrationNeeded: true)
      
      #else
      if let _ = NSClassFromString("XCTest") {
        return Realm.Configuration(fileURL: self.settingsURL, inMemoryIdentifier: "LifecakeSettingsRealm", deleteRealmIfMigrationNeeded: true)
      } else {
        return Realm.Configuration(fileURL: self.settingsURL, schemaVersion: self.schemaVersion, deleteRealmIfMigrationNeeded: true)
      }
      #endif
    }()
    
    return configuration
  }
  
  // MARK: - Breacrumbs
  
  // simplified breadcrumb that is shared between app and extension
  fileprivate func RMLeaveBreadcrumb(_ fnc: String = #function, line: Int = #line, comment: String = "") {
    
    let breadcrumb = "RealmManager \(fnc)(\(line))" + (comment.isEmpty ? "" : " - \(comment)")
    print(breadcrumb)
    
    #if !DEBUG
      Crashlytics.crashlytics().log(format: "%@", arguments: getVaList([breadcrumb]))
    #endif
  }
}

