//
//  RealmUpdateEquatable.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 13/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation

protocol RealmUpdateEquatable {
  
  func equalsToRealm(value: Any) -> Bool
}

extension String: RealmUpdateEquatable {
  
  func equalsToRealm(value: Any) -> Bool {
    return self == value as? String
  }
}

extension Bool: RealmUpdateEquatable {
  
  func equalsToRealm(value: Any) -> Bool {
    return self == value as? Bool
  }
}

extension Int: RealmUpdateEquatable {
  
  func equalsToRealm(value: Any) -> Bool {
    return self == (value as? Int)
  }
}

extension TimeInterval: RealmUpdateEquatable {
  
  func equalsToRealm(value: Any) -> Bool {
    return self == (value as? TimeInterval)
  }
}
