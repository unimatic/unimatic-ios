//
//  RealmNotificationsHandler.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 13/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation
import RealmSwift

class RealmNotificationsHandler {
  
  static var tokens = [String: NotificationToken]()
  
  class func finishUpdate(forKey key: String, result: Result<Bool, ErrorResponse>, completion: @escaping (Result<Bool, ErrorResponse>) -> ()) {
    
    let token = RealmNotificationsHandler.tokens[key]
    token?.invalidate()
    
    RealmNotificationsHandler.tokens.removeValue(forKey: key)
    completion(result)
  }
}
