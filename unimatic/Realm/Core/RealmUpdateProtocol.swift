//
//  RealmUpdateProtocol.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 13/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmUpdateProtocol {
  
  var primaryId: String { get }
  
  associatedtype Property: RawRepresentable, Hashable
  
  /*
   Update without completion block is suitable for using on background threads
   User Realm.refresh() to get latest changes while switching between threads
   */
  func update(values: [Property: RealmUpdateEquatable])
  
  /*
   Committed does not mean that current thread has latest version of realm objects
   in order to have latest version, use .refresh or objects notification block.
   For more info please check:
   * https://realm.io/docs/swift/latest/#seeing-changes-from-other-threads
   * https://realm.io/docs/swift/latest/#notifications
   */
  func update(values: [Property: RealmUpdateEquatable], committed: @escaping (Result<Bool, ErrorResponse>) -> ())
  
  /*
   Completion ensures, that callback happens only when item was updated on main thread.
   Should be called only from main thread, otherwise crash
   Use it only for quick updates and when your UI depends from those changes. 
   Having multiple updates happening at the same time slows down other realm notifications
   Consider to use update + committed whenever possible
   */
  func update(values: [Property: RealmUpdateEquatable], completion: @escaping (Result<Bool, ErrorResponse>) -> ())
}

// MARK: - RealmUpdateProtocol

extension RealmUpdateProtocol where Property.RawValue == String {
  
  func update(values: [Property: RealmUpdateEquatable]) {
    
    guard let this = (self as? Object), !this.isInvalidated else {
      return
    }
    
    let id = self.primaryId
    let type = Swift.type(of: this)
    
    Realm.write { realm in
      self.performWrite(realm: realm, id: id, type: type, values: values)
    }
  }
  
  func update(values: [Property: RealmUpdateEquatable], committed: @escaping (Result<Bool, ErrorResponse>) -> ()) {
    
    guard let this = (self as? Object), !this.isInvalidated else {
      committed(.failure(ErrorResponse(type: .realmInvalidated)))
      return
    }
    
    let id = self.primaryId
    let type = Swift.type(of: this)

    Realm.async(write: { (realm) in
      self.performWrite(realm: realm, id: id, type: type, values: values)
    }) {
      committed(.success(true))
    }
  }
  
  func update(values: [Property: RealmUpdateEquatable], completion: @escaping (Result<Bool, ErrorResponse>) -> ()) {
    
    guard let this = (self as? Object), !this.isInvalidated else {
      completion(.failure(ErrorResponse(type: .realmInvalidated)))
      return
    }
    
    let key = String.unique()
    let id = self.primaryId
    let type = Swift.type(of: this)
    
    Realm.async(write: { (realm) in
      self.performWrite(realm: realm, id: id, type: type, values: values, setupNotifications: { values in
        
        Async.main {
          
          if values.isEmpty {
            RealmNotificationsHandler.finishUpdate(forKey: key, result: .success(true), completion: completion)

          } else {
            
            var valuesToChange = values
            
            RealmNotificationsHandler.tokens[key] = this.observe { change in
              
              switch change {
              case .change(_, let properties):
                properties.forEach {
                  if let newValue = $0.newValue, let requestedValue = valuesToChange[$0.name], requestedValue.equalsToRealm(value: newValue) {
                    valuesToChange.removeValue(forKey: $0.name)
                  }
                }
                
              case .error(let error):
                RealmNotificationsHandler.finishUpdate(forKey: key, result: .failure(ErrorResponse(error: error)), completion: completion)
                return
                
              case .deleted:
                RealmNotificationsHandler.finishUpdate(forKey: key, result: .failure(ErrorResponse(type: .realmInvalidated)), completion: completion)
                return
              }
              
              if valuesToChange.isEmpty {
                RealmNotificationsHandler.finishUpdate(forKey: key, result: .success(true), completion: completion)
              }
            }
          }
        }
      })
    })
  }
  
  // MARK: - Help functions
  
  fileprivate func performWrite(realm: Realm, id: String, type: Object.Type, values: [Property: RealmUpdateEquatable], setupNotifications: ((_ values: [String: RealmUpdateEquatable]) -> ())? = nil) {
    
    var newValues = [String: RealmUpdateEquatable]()
    
    // item could get invalidated in meantime
    if let itemToUpdate = realm.object(ofType: type, forPrimaryKey: id) {
      
      // update only changed values to avoid calculating and
      // calling notifications around app
      
      values.forEach { (key, newValue) in
        
        let oldValue = itemToUpdate.value(forKey: key.rawValue)
        
        if oldValue == nil || (oldValue != nil && !newValue.equalsToRealm(value: oldValue!)) {
          newValues[key.rawValue] = newValue
        }
      }
    }
    
    setupNotifications?(newValues)
    
    if !newValues.isEmpty {
      newValues["id"] = id
      realm.create(type, value: newValues, update: .all)
    }
  }
}

