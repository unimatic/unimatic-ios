//
//  Realm+Location.swift
//  unimatic
//
//  Created by Filippo Tosetto on 03/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import RealmSwift
import MapKit

extension Realm {
  
  static func getLocations() -> [RMLocation] {
    return Array((try! Realm()).objects(RMLocation.self))
  }
  
  static func getOtherLocations() -> [RMLocation] {
    return Array((try! Realm()).objects(RMLocation.self).filter(NSPredicate(format: "current == false")))
  }
  
  static func getCurrentLocation() -> RMLocation? {
    return Realm.objects(RMLocation.self).first(where: { $0.current == true })
  }
  
  static func addNewLocation(withName name: String, andPlacemark placemark: MKPlacemark, timezone: TimeZone?) {
    let realm = try! Realm()
    try! realm.write {
      let location = RMLocation(title: name, center: placemark.coordinate, timeZone: timezone)
      realm.add(location)
    }
  }
  
  static func setCurrentLocation(location: RMLocation) {
    let currentLocation = Realm.getCurrentLocation()
    
    let realm = try! Realm()
    try! realm.write {
      if let current = currentLocation {
        realm.delete(current)
      }
      location.current = true
      realm.add(location)
    }
  }
  
  static func deleteLocation(location: RMLocation) {
    let realm = try! Realm()
    try! realm.write {
      realm.delete(location)
    }
  }
}
