//
//  Realm+Extensions.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 13/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import RealmSwift

extension Realm {
  
  static let SerialQueue = DispatchQueue(label: "RealmCustomQueue")
  static let ConcurrentQueue = DispatchQueue(label: "RealmCustomQueue2", attributes: .concurrent)
  
  enum Priority {
    case Low, High
  }
  
  static func objects<T: Object>(_ type: T.Type) -> Results<T> {
    return (try! Realm()).objects(type)
  }
  
  static func object<T: Object, K>(ofType type: T.Type, forPrimaryKey key: K) -> T? {
    return (try! Realm()).object(ofType: type, forPrimaryKey: key)
  }
  
  static func refresh() {
    (try! Realm()).refresh()
  }
  
  static func read(block: (_ realm: Realm) -> ()) {
    block(try! Realm())
  }
  
  static func write(block: (_ realm: Realm) -> ()) {
    
    let realm = try! Realm()
    try! realm.write {
      block(realm)
    }
  }
  
  static func add(_ object: Object) {
    
    Realm.write { realm in
      realm.add(object)
    }
  }
  
  static func delete(_ object: Object) {
    
    let realm = try! Realm()
    try! realm.write {
      realm.delete(object)
    }
  }
  
  static func deleteAllRealms() {
    
    let realm = try! Realm()
    try! realm.write {
      realm.deleteAll()
    }
  }
  
  static func async(priority: Realm.Priority = .Low, write: @escaping (_ realm: Realm) -> (), writeCommitted: (() -> ())? = nil) {
    
    let queue = priority == .High ? Realm.ConcurrentQueue : Realm.SerialQueue
    
    queue.async {
      
      autoreleasepool {
        let realm = try! Realm()
        try! realm.write {
          write(realm)
        }
      }
      
      DispatchQueue.main.async {
        writeCommitted?()
      }
    }
  }
  
  func deleteObject<T: Object, K>(ofType type: T.Type, forPrimaryKey key: K) {
    
    if let task = self.object(ofType: type, forPrimaryKey: key) {
      self.delete(task)
    }
  }
}
