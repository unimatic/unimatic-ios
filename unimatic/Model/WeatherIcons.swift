//
//  WeatherIcons.swift
//  unimatic
//
//  Created by Filippo Tosetto on 05/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation


enum WeatherIcons {
  case clearSky(day: Bool), fewClouds(day: Bool)
  case scatteredClouds, brokenClouds
  case showerRain, rain(day: Bool)
  case thunderstorm, snow, mist
  
  var systemSymbol: String {
  
    switch self {
    case .clearSky(let day):
      return day ? "sun.max" : "moon"
      
    case .fewClouds(let day):
      return day ? "cloud.sun" : "cloud.moon"
      
    case .scatteredClouds:
      return "cloud"
      
    case .brokenClouds:
      return "smoke"

    case .showerRain:
      return "cloud.rain"
      
    case .rain(let day):
      return day ? "cloud.sun.rain" : "cloud.moon.rain"
      
    case .thunderstorm:
      return "cloud.bold"
      
    case .snow:
      return "snow"
      
    case .mist:
      return "cloud.fog"
    }
  }
}

extension WeatherIcons {
  
  init?(value: String) {
    switch value {
    case "01d":
      self = .clearSky(day: true)
      
    case "01n":
      self = .clearSky(day: false)
      
    case "02d":
      self = .fewClouds(day: true)
      
    case "02n":
      self = .fewClouds(day: false)
      
    case "03d", "03n":
      self = .scatteredClouds
      
    case "04d", "04n":
      self = .brokenClouds
      
    case "09d", "09n":
      self = .showerRain
      
    case "10d":
      self = .rain(day: true)
      
    case "10n":
      self = .rain(day: false)
      
    case "11d", "11n":
      self = .thunderstorm
      
    case "13d", "13n":
      self = .snow

    case "50d", "50n":
      self = .mist
      
    default: return nil
    }
  }
}
