//
//  LapViewModel.swift
//  unimatic
//
//  Created by Filippo Tosetto on 30/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct LapViewModel {
  
  var id: String
  var lap: Lap
  var lapNumber: Int
  
  var difference: Int
  
  var isBest: Bool
  
  init(id: String = String.unique(), lap: Lap, lapNumber: Int, difference: Int = 0, isBest: Bool = false) {
    self.id = id
    self.lap = lap
    self.lapNumber = lapNumber
    self.difference = difference
    self.isBest = isBest
  }
  
  func getDifferenceDescription() -> String {
    
    let fraction = self.difference%100
    let totalSeconds = self.difference/100

    let seconds = totalSeconds%60
    let totalMinutes = totalSeconds/60
    let minutes = totalMinutes%60
    let totalHours = totalMinutes/60
    
    let fractionString = fraction > 9 ? "\(fraction)" : "0\(fraction)"
    let secondsString = seconds > 9 ? "\(seconds)" : "0\(seconds)"
    let minutesString = minutes > 9 ? "\(minutes)" : "0\(minutes)"

    return "+\(totalHours):\(minutesString):\(secondsString).\(fractionString)"
  }
  
  func setDifference(fromLap bestLap: LapViewModel) -> LapViewModel {
    return LapViewModel(id: self.id,
                        lap: self.lap,
                        lapNumber: self.lapNumber,
                        difference: self.lap.counter - bestLap.lap.counter,
                        isBest: self == bestLap)
  }
  
  func setIsBest(best: Bool) -> LapViewModel {
    return LapViewModel(id: self.id,
                        lap: self.lap,
                        lapNumber: self.lapNumber,
                        difference: self.difference,
                        isBest: best)
  }
}

// MARK: - Comparable

extension LapViewModel: Comparable {
  static func < (lhs: LapViewModel, rhs: LapViewModel) -> Bool {
    return lhs.lap.counter < rhs.lap.counter
  }
  
  
  static func == (lhs: LapViewModel, rhs: LapViewModel) -> Bool {
    return lhs.id == rhs.id
  }
}
