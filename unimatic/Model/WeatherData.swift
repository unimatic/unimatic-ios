//
//  WeatherData.swift
//  unimatic
//
//  Created by Filippo Tosetto on 27/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct WeatherData: Decodable {
  var city: City
  var list: [DailyWeather]
}

struct City: Decodable {
  var name: String
  var timezone: Int
}

struct DailyWeather: Decodable {
  var dt: Int
  var sunrise: Int
  var sunset: Int
  
  var temp: Temperature
  var weather: [Weather]
}

struct Temperature: Decodable {
  var day: Double
  var min: Double
  var max: Double
  var night: Double
  var eve: Double
  var morn: Double
  
}

struct Weather: Decodable {
  var main: String
  var icon: String
}
