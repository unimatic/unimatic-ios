//
//  Lap.swift
//  unimatic
//
//  Created by Filippo Tosetto on 30/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct Lap {
  var hours: Int
  var minutes: Int
  var seconds: Int
  
  var fractions: Int
  
  var counter: Int
  
  mutating func setToZero()  {
    self.hours = 0
    self.minutes = 0
    self.seconds = 0
    self.fractions = 0
    
    self.counter = 0
  }
  
  func getDescription() -> String {
    let secondsString = self.seconds > 9 ? "\(self.seconds)" : "0\(self.seconds)"
    let minutesString = self.minutes > 9 ? "\(self.minutes)" : "0\(self.minutes)"
    let hoursString = self.hours > 9 ? "\(self.hours)" : "0\(self.hours)"
    
    return "\(hoursString):\(minutesString):\(secondsString)"
  }
  
  func getFullDescription() -> String {
    let fractionsString = self.fractions > 9 ? "\(self.fractions)" : "0\(self.fractions)"

    let secondsString = self.seconds > 9 ? "\(self.seconds)" : "0\(self.seconds)"
    let minutesString = self.minutes > 9 ? "\(self.minutes)" : "0\(self.minutes)"
    let hoursString = self.hours > 9 ? "\(self.hours)" : "0\(self.hours)"
    
    return "\(hoursString):\(minutesString):\(secondsString).\(fractionsString)"
  }

  mutating func update() {
    self.counter += 1
    self.fractions += 1
    
    if self.fractions > 99 {
      self.seconds += 1
      self.fractions = 0
    }
    
    if self.seconds == 60 {
      self.minutes += 1
      self.seconds = 0
    }
    
    if self.minutes == 60 {
      self.hours += 1
      self.minutes = 0
    }
  }

}
