//
//  LapsContainer.swift
//  unimatic
//
//  Created by Filippo Tosetto on 30/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct LapsContainer {
  var lapViewModels: [LapViewModel]
  
  init() {
    self.lapViewModels = [LapViewModel]()
  }
  
  func count() -> Int {
    return self.lapViewModels.count
  }
  
  func object(atIndex index: IndexPath) -> LapViewModel {
    return self.lapViewModels[index.row]
  }
  
  mutating func addNewLap(lap: Lap) {
    self.lapViewModels.append(LapViewModel(lap: lap, lapNumber: self.lapViewModels.count + 1))

    if let best = self.lapViewModels.min(by: { $0 < $1 } )?.setIsBest(best: true) {
      self.lapViewModels = self.lapViewModels.map { $0.setDifference(fromLap: best) }
    }
  }
  
  mutating func cleanup() {
    self.lapViewModels = [LapViewModel]()
  }
}
