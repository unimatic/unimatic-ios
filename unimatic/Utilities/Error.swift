//
//  Error.swift
//  unimatic
//
//  Created by Filippo Tosetto on 01/06/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation

enum ErrorCode: Int {
  
  // MARK: - Network errors
  
  case cancelled = -999
  case unknown = 0
  case generalError = 1001
  case authenticationError = 1002
  case missingSession = 1003
  case invalidJSON = 1004
  case uploadFailed = 1005
  case invalidSession = 1006
  case permissionError = 1007
  case noMatchError = 1008
  case invalidRequest = 1009
  case emailExists = 1011
  case usernameExist = 1012
  case invalidClient = 1013
  case invalidFile = 1014
  case tooManyResends = 1015
  case tooRecentResend = 1016
  case invalidEncryption = 1020
  case wrongEncryption  = 1021
  case verifyFailed = 1022
  case serviceDisabled = 1023
  case callOutDated = 1024
  case uploadExceedsSize = 1025
  case sharingError = 1026
  case storageQuotaError = 1027
  case binaryMissing = 1028
  case importURLInvalid = 1029
  case s3FileZeroLength = 1038
  case s3FileNonValid = 1040
  case binaryInvlaidSize = 1041
  case binaryInvalidMD5 = 1042
  case deviceMismatch = 1089
  case invalidConfiguration = 1095
  case refreshTokenMissing = 3010
  case unexpectedReponse = 40001
  case authEmailPermissionNotGiven = 20102
  
  // MARK: - Local errors. 4xxx reserved for client errors

  case realmInvalidated = 4000
  case noAssetToUpload = 4004
  case noFileURLToUpload = 4005
  case noDataToUpload = 4006
  case noVideoToUpload = 4007
  case noDataFilesize = 4008
  case applePaymentNoReceipt = 4009
  case applePaymentInvalid = 4010
  case imageSaveFailed = 4013
  case subscriptionFailed = 4018
  case facebookDisabled = 4019
  case appleDisabled = 4020
  
  case stripeErrorCreatingCustomer = 4021
  case stripeErrorCreatingPaymentIntent = 4022
  case stripeErrorAuthenticatingCard = 4023
  
  init(code: Int?) {
    guard let code = code, let errorCode = ErrorCode(rawValue: code) else {
      self = .unknown
      return
    }
    
    self = errorCode
  }
  
  var description: String? {
    
    // Add UI tests to ErrorResponse flows
    
    switch self {
    case .authenticationError: return "generic_error_autenticationfailed".localized
    case .unexpectedReponse: return "Unexpected reponse format from server"
    case .authEmailPermissionNotGiven: return "generic_error_permissionisneeded".localized
    case .refreshTokenMissing: return "generic_error_socialimport_accountalreadyinuse".localized
    case .subscriptionFailed: return "settings_storage_purchase_error_failed_purchase".localized
    case .facebookDisabled: return "homepage_error_facebook_disabled".localized
    case .appleDisabled: return "homepage_error_apple_disabled".localized
      
    case .stripeErrorCreatingCustomer: return "Error 3D authentication"
    case .stripeErrorCreatingPaymentIntent: return "Error creating paymetn intent"
    case .stripeErrorAuthenticatingCard: return "Error Authenticating card"
    
    default: return nil
    }
  }
}

struct ErrorResponse: Swift.Error {
  
  var message: String?
  var code: Int?
  var httpStatus: Int?
  
  var type: ErrorCode {
    return ErrorCode(code: self.code)
  }
  
  var isCancelled: Bool {
    return self.code == ErrorCode.cancelled.rawValue
  }
  
  // MARK: - class
  
  init(error: Error?, response: HTTPURLResponse? = nil) {

    if let headers = response?.allHeaderFields, let statusCode = response?.statusCode {
      self.code = Int((headers as NSDictionary).stringForCaseInsensitiveKey("ErrorCode" as AnyObject?) ?? "0")
      if let message = (headers as NSDictionary).stringForCaseInsensitiveKey("Error" as AnyObject?) {
        self.message = message
      }
      
      self.httpStatus = statusCode
      
      if httpStatus == 401 {
        // temporarily map 401 to 1006, since that's what should be the equivalent lifecake error code
        self.code = 1006
        
      } else if self.code == nil && self.httpStatus != nil {
        // if code doesn't exist in header, but we have an http status code, make that the code
        self.code = httpStatus
      }
      
      if let code = self.code, let errorCodeDescription = ErrorCode(rawValue: code)?.description {
        self.message = errorCodeDescription
        
      } else if let localizedDescription = error?.localizedDescription , self.message == nil {
        self.message = localizedDescription
      }
      
    } else {
      self.code = (error as NSError?)?.code
      self.message = error?.localizedDescription
    }
  }
  
  init(type: ErrorCode) {
    self.code = type.rawValue
    self.message = type.description
  }
  
  // MARK: - Object
  
  var description: String {
    return "code: \(String(describing: self.code)), message: \(String(describing: self.message))"
  }
  
  init(code: Int?, message: String?) {
    self.code = code
    self.message = message
  }
}
