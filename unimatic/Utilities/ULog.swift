//
//  ULog.swift
//  unimatic
//
//  Created by Filippo Tosetto on 08/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation
import Firebase
import FirebaseCrashlytics

enum ULogType {
  case net, assert, error, upload, revenue, info
  
  func boolValue() -> Bool {
    
    switch self {
      
    case .assert:
      return false
      
    case .net:
      return false
      
    case .error:
      return false
      
    case .upload:
      return false
    
    case .revenue:
      return false
    
    case .info:
      return true
      
    }
  }
  
  func symbol() -> String {
    
    switch self {

    case .assert:
      return "📛"
      
    case .net:
      return "🌏"
      
    case .error:
      return "❌"
      
    case .upload:
      return "📤"
    
    case .revenue:
      return "💰"
      
    case .info:
      return "🔧"
    }
  }

}


func ULog(
  _ string: String,
  type: ULogType = .info,
  file: String = #file,
  fnc: String = #function,
  line: Int = #line) {

    if type.boolValue() {
    #if DEBUG
    
      let fileName = file.lastPathComponent.deletePathExtension
      let message = "\(type.symbol()) [\(fileName): \(fnc)(\(line)]: \(string)\n"
      print(message)
    #endif
    }
}

func ULog(
  _ object: Any,
  type: ULogType = .info,
  file: String = #file,
  fnc: String = #function,
  line: Int = #line) {

    if type.boolValue() {
    #if DEBUG
    
      let fileName = file.lastPathComponent.deletePathExtension
      let message = "\(type.symbol()) [\(fileName): \(fnc)(\(line)]: \(object)\n"
      print(message)
    #endif
    }
}

func IRLog(_ string: String,
           file: String = #file,
           fnc: String = #function,
           line: Int = #line) {
  ULog(string, type: .revenue, file: file, fnc: fnc, line: line)
}

func IULog(_ string: String,
  file: String = #file,
  fnc: String = #function,
  line: Int = #line) {
    ULog(string, type: .upload, file: file, fnc: fnc, line: line)
}

func INETLog(_ string: String,
  file: String = #file,
  fnc: String = #function,
  line: Int = #line) {
    ULog(string, type: .net, file: file, fnc: fnc, line: line)
}

func IErrorLog(_ string: String,
  file: String = #file,
  fnc: String = #function,
  line: Int = #line) {
    ULog(string, type: .error, file: file, fnc: fnc, line: line)
}

func IErrorLog(_ error: Error,
  file: String = #file,
  fnc: String = #function,
  line: Int = #line) {
    ULog(error, type: .error, file: file, fnc: fnc, line: line)
}

func IAssertLog(_ string: String,
                file: String = #file,
                fnc: String = #function,
                line: Int = #line) {
  
  #if DEBUG
    assertionFailure(string)
  
    ULog(string, type: .assert, file: file, fnc: fnc, line: line)

  #else
    let error = NSError(domain: "Buddyfit", code: 0, userInfo: [NSLocalizedDescriptionKey: string])
    Crashlytics.crashlytics().record(error: error)
  #endif
}


func LeaveBreadcrumb(_ file: String = #file, fnc: String = #function, line: Int = #line, comment: String = "") {
  
  let fileName = {
    if !file.contains(".") {
      return file
    } else {
      return file.lastPathComponent.deletePathExtension
    }
  }() as String

  let breadcrumb = "\(fileName) \(fnc)(\(line))" + (comment.isEmpty ? "" : " - \(comment)")
  
  #if !DEBUG
    Crashlytics.crashlytics().log(format: "%@", arguments: getVaList([breadcrumb]))
  #endif
}
