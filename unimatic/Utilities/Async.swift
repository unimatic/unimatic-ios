//
//  Async.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 18/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation

private enum GCD {
  case main, userInteractive, userInitiated, utility, background, custom(queue: DispatchQueue)

  var queue: DispatchQueue {
    switch self {
    case .main: return .main
    case .userInteractive: return .global(qos: .userInteractive)
    case .userInitiated: return .global(qos: .userInitiated)
    case .utility: return .global(qos: .utility)
    case .background: return .global(qos: .background)
    case .custom(let queue): return queue
    }
  }
}


private class Reference<T> {
  var value: T?
}

typealias Async = AsyncBlock<Void, Void>

struct AsyncBlock<In, Out> {


  // MARK: - Private properties and init

  /**
   Private property to hold internally on to a `@convention(block) () -> Swift.Void`
  */
  private let block: DispatchWorkItem

  private let input: Reference<In>?
  private let output_: Reference<Out>
  public var output: Out? {
    return output_.value
  }

  private init(_ block: DispatchWorkItem, input: Reference<In>? = nil, output: Reference<Out> = Reference()) {
    self.block = block
    self.input = input
    self.output_ = output
  }

  @discardableResult
  public static func main<O>(after seconds: Double? = nil, _ block: @escaping () -> O) -> AsyncBlock<Void, O> {
    return AsyncBlock.async(after: seconds, block: block, queue: .main)
  }
  
  @discardableResult
  public static func background<O>(after seconds: Double? = nil, _ block: @escaping () -> O) -> AsyncBlock<Void, O> {
      return Async.async(after: seconds, block: block, queue: .background)
  }

  @discardableResult
  public static func custom<O>(queue: DispatchQueue, after seconds: Double? = nil, _ block: @escaping () -> O) -> AsyncBlock<Void, O> {
      return Async.async(after: seconds, block: block, queue: .custom(queue: queue))
  }


  // MARK: - Private static methods

  /**
   Convenience for dispatch_async(). Encapsulates the block in a "true" GCD block using DISPATCH_BLOCK_INHERIT_QOS_CLASS.

   - parameters:
       - block: The block that is to be passed to be run on the `queue`
       - queue: The queue on which the `block` is run.

   - returns: An `Async` struct which encapsulates the `@convention(block) () -> Swift.Void`
   */

  private static func async<O>(after seconds: Double? = nil, block: @escaping () -> O, queue: GCD) -> AsyncBlock<Void, O> {
      let reference = Reference<O>()
      let block = DispatchWorkItem(block: {
          reference.value = block()
      })

      if let seconds = seconds {
          let time = DispatchTime.now() + seconds
          queue.queue.asyncAfter(deadline: time, execute: block)
      } else {
          queue.queue.async(execute: block)
      }

      // Wrap block in a struct since @convention(block) () -> Swift.Void can't be extended
      return AsyncBlock<Void, O>(block, output: reference)
  }

  @discardableResult
  func main<O>(after seconds: Double? = nil, _ chainingBlock: @escaping (Out) -> O) -> AsyncBlock<Out, O> {
    return chain(after: seconds, block: chainingBlock, queue: .main)
  }

  @discardableResult
  func background<O>(after seconds: Double? = nil, _ chainingBlock: @escaping (Out) -> O) -> AsyncBlock<Out, O> {
    return chain(after: seconds, block: chainingBlock, queue: .background)
  }

  @discardableResult
  func custom<O>(queue: DispatchQueue, after seconds: Double? = nil, _ chainingBlock: @escaping (Out) -> O) -> AsyncBlock<Out, O> {
    return chain(after: seconds, block: chainingBlock, queue: .custom(queue: queue))
  }

    // MARK: - Instance methods

    /**
    Convenience function to call `dispatch_block_cancel()` on the encapsulated block.
    Cancels the current block, if it hasn't already begun running to GCD.

    Usage:

        let block1 = Async.background {
            // Some work
        }
        let block2 = block1.background {
            // Some other work
        }
        Async.main {
            // Cancel async to allow block1 to begin
            block1.cancel() // First block is NOT cancelled
            block2.cancel() // Second block IS cancelled
        }

    */
  func cancel() {
    self.block.cancel()
  }

  @discardableResult
  func wait(seconds: Double? = nil) -> DispatchTimeoutResult {
    let timeout = seconds.flatMap { DispatchTime.now() + $0 } ?? .distantFuture
    return self.block.wait(timeout: timeout)
  }

  private func chain<O>(after seconds: Double? = nil, block chainingBlock: @escaping (Out) -> O, queue: GCD) -> AsyncBlock<Out, O> {
    let reference = Reference<O>()
    let dispatchWorkItem = DispatchWorkItem(block: {
        reference.value = chainingBlock(self.output_.value!)
    })

    let queue = queue.queue
    if let seconds = seconds {
      self.block.notify(queue: queue) {
            let time = DispatchTime.now() + seconds
            queue.asyncAfter(deadline: time, execute: dispatchWorkItem)
        }
    } else {
      self.block.notify(queue: queue, execute: dispatchWorkItem)
    }

    // See Async.async() for comments
    return AsyncBlock<Out, O>(dispatchWorkItem, input: self.output_, output: reference)
  }
}


// MARK: - Apply - DSL for `dispatch_apply`

/**
`Apply` is an empty struct with convenience static functions to parallelize a for-loop, as provided by `dispatch_apply`.

    Apply.background(100) { i in
        // Calls blocks in parallel
    }

`Apply` runs a block multiple times, before returning. If you want run the block asynchronously from the current thread, wrap it in an `Async` block:

    Async.background {
        Apply.background(100) { i in
            // Calls blocks in parallel asynchronously
        }
    }

- SeeAlso: Grand Central Dispatch, dispatch_apply
*/
public struct Apply {

  static func background(_ iterations: Int, block: @escaping (Int) -> ()) {
    GCD.background.queue.async {
      DispatchQueue.concurrentPerform(iterations: iterations, execute: block)
    }
  }

  static func custom(queue: DispatchQueue, iterations: Int, block: @escaping (Int) -> ()) {
    queue.async {
      DispatchQueue.concurrentPerform(iterations: iterations, execute: block)
    }
  }
}


// MARK: - AsyncGroup – Struct

/**
The **AsyncGroup** struct facilitates working with groups of asynchronous blocks. Handles a internally `dispatch_group_t`.

Multiple dispatch blocks with GCD:

    let group = AsyncGroup()
    group.background {
        // Run on background queue
    }
    group.utility {
        // Run on untility queue, after the previous block
    }
    group.wait()

All moderns queue classes:

    group.main {}
    group.userInteractive {}
    group.userInitiated {}
    group.utility {}
    group.background {}

Custom queues:

    let customQueue = dispatch_queue_create("Label", DISPATCH_QUEUE_CONCURRENT)
    group.customQueue(customQueue) {}

Wait for group to finish:

    let group = AsyncGroup()
    group.background {
        // Do stuff
    }
    group.background {
        // Do other stuff in parallel
    }
    // Wait for both to finish
    group.wait()
    // Do rest of stuff

- SeeAlso: Grand Central Dispatch
*/
public struct AsyncGroup {

  private var group: DispatchGroup

  init() {
    self.group = DispatchGroup()
  }

  private func async(block: @escaping @convention(block) () -> Swift.Void, queue: GCD) {
    queue.queue.async(group: self.group, execute: block)
  }

  func enter() {
    self.group.enter()
  }

  func leave() {
    self.group.leave()
  }

  func main(_ block: @escaping @convention(block) () -> Swift.Void) {
    self.async(block: block, queue: .main)
  }

  func background(_ block: @escaping @convention(block) () -> Swift.Void) {
    self.async(block: block, queue: .background)
  }

  func custom(queue: DispatchQueue, block: @escaping @convention(block) () -> Swift.Void) {
    self.async(block: block, queue: .custom(queue: queue))
  }

  @discardableResult
  func wait(seconds: Double? = nil) -> DispatchTimeoutResult {
    let timeout = seconds.flatMap { DispatchTime.now() + $0 } ?? .distantFuture
    return self.group.wait(timeout: timeout)
  }
}


// MARK: - Extension for `qos_class_t`

/**
Extension to add description string for each quality of service class.
*/
public extension qos_class_t {

  var description: String {
    get {
      switch self {
      case qos_class_main(): return "Main"
      case DispatchQoS.QoSClass.default.rawValue: return "Default"
      case DispatchQoS.QoSClass.background.rawValue: return "Background"
      case DispatchQoS.QoSClass.unspecified.rawValue: return "Unspecified"
      default: return "Unknown"
      }
    }
  }
}


// MARK: - Extension for `DispatchQueue.GlobalAttributes`

/**
 Extension to add description string for each quality of service class.
 */
extension DispatchQoS.QoSClass {

  var description: String {
    get {
      switch self {
      case DispatchQoS.QoSClass(rawValue: qos_class_main())!: return "Main"
      case .default: return "Default"
      case .background: return "Background"
      case .unspecified: return "Unspecified"
      case .utility: return "Utility"
      case .userInitiated: return "UserInitiated"
      case .userInteractive: return "userInteractive"
      @unknown default: return "Default"
      }
    }
  }
}

