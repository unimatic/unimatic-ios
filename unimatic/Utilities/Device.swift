//
//  Device.swift
//  Buddyfit
//
//  Created by Filippo Tosetto on 13/05/2020.
//  Copyright © 2020 Tripix Tech Srl. All rights reserved.
//

import Foundation

import UIKit

// https://github.com/dennisweissmann/DeviceKit
public enum Device {
  
  case iPodTouch5
  case iPodTouch6
  case iPhone4
  case iPhone4s
  case iPhone5
  case iPhone5c
  case iPhone5s
  case iPhone6
  case iPhone6Plus
  case iPhone6s
  case iPhone6sPlus
  case iPhone7
  case iPhone7Plus
  case iPhoneSE
  case iPhone8
  case iPhone8Plus
  case iPhoneX
  case iPhoneXs
  case iPhoneXsMax
  case iPhoneXr
  case iPhone11
  case iPhone11Pro
  case iPhone11ProMax
  case iPad2
  case iPad3
  case iPad4
  case iPadAir
  case iPadAir2
  case iPad5
  case iPad6
  case iPad7
  case iPadMini
  case iPadMini2
  case iPadMini3
  case iPadMini4
  case iPadMini5
  case iPadPro9Inch
  case iPadPro12Inch
  case iPadPro12Inch2
  case iPadPro10Inch
  case iPadPro11Inch
  case iPadPro12Inch3
  
  indirect case simulator(Device)
  case unknown(String)
  
  public init() {
    self = Device.instance
  }
  
  /// Gets the identifier from the system, such as "iPhone7,1".
  public static var identifier: String = {
    var systemInfo = utsname()
    uname(&systemInfo)
    let mirror = Mirror(reflecting: systemInfo.machine)
    
    let identifier = mirror.children.reduce("") { identifier, element in
      guard let value = element.value as? Int8, value != 0 else { return identifier }
      return identifier + String(UnicodeScalar(UInt8(value)))
    }
    
    return identifier
  }()
  
  private static let instance = Device.mapToDevice(identifier: Device.identifier)
  
  /// Maps an identifier to a Device. If the identifier can not be mapped to an existing device, `UnknownDevice(identifier)` is returned.
  ///
  /// - parameter identifier: The device identifier, e.g. "iPhone7,1". Can be obtained from `Device.identifier`.
  ///
  /// - returns: An initialized `Device`.
  public static func mapToDevice(identifier: String) -> Device {
    
    switch identifier {
    case "iPod5,1": return iPodTouch5
    case "iPod7,1": return iPodTouch6
    case "iPhone3,1", "iPhone3,2", "iPhone3,3": return iPhone4
    case "iPhone4,1": return iPhone4s
    case "iPhone5,1", "iPhone5,2": return iPhone5
    case "iPhone5,3", "iPhone5,4": return iPhone5c
    case "iPhone6,1", "iPhone6,2": return iPhone5s
    case "iPhone7,2": return iPhone6
    case "iPhone7,1": return iPhone6Plus
    case "iPhone8,1": return iPhone6s
    case "iPhone8,2": return iPhone6sPlus
    case "iPhone9,1", "iPhone9,3": return iPhone7
    case "iPhone9,2", "iPhone9,4": return iPhone7Plus
    case "iPhone8,4": return iPhoneSE
    case "iPhone10,1", "iPhone10,4": return iPhone8
    case "iPhone10,2", "iPhone10,5": return iPhone8Plus
    case "iPhone10,3", "iPhone10,6": return iPhoneX
    case "iPhone11,2": return iPhoneXs
    case "iPhone11,4", "iPhone11,6": return iPhoneXsMax
    case "iPhone11,8": return iPhoneXr
    case "iPhone12,1": return iPhone11
    case "iPhone12,3": return iPhone11Pro
    case "iPhone12,5": return iPhone11ProMax
    case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4": return iPad2
    case "iPad3,1", "iPad3,2", "iPad3,3": return iPad3
    case "iPad3,4", "iPad3,5", "iPad3,6": return iPad4
    case "iPad4,1", "iPad4,2", "iPad4,3": return iPadAir
    case "iPad5,3", "iPad5,4": return iPadAir2
    case "iPad6,11", "iPad6,12": return iPad5
    case "iPad7,5", "iPad7,6": return iPad6
      
    case "iPad9,1": return iPad7 // TODO
      
    case "iPad2,5", "iPad2,6", "iPad2,7": return iPadMini
    case "iPad4,4", "iPad4,5", "iPad4,6": return iPadMini2
    case "iPad4,7", "iPad4,8", "iPad4,9": return iPadMini3
    case "iPad5,1", "iPad5,2": return iPadMini4
    case "iPad11,1", "iPad11,2": return iPadMini4
    case "iPad6,3", "iPad6,4": return iPadPro9Inch
    case "iPad6,7", "iPad6,8": return iPadPro12Inch
    case "iPad7,1", "iPad7,2": return iPadPro12Inch2
    case "iPad7,3", "iPad7,4": return iPadPro10Inch
    case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4": return iPadPro11Inch
    case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8": return iPadPro12Inch3
    case "i386", "x86_64": return simulator(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))
    default: return unknown(identifier)
    }
  }
  
  /// Get the real device from a device.
  /// If the device is a an iPhone8Plus simulator this function returns .iPhone8Plus (the real device).
  /// If the parameter is a real device, this function returns just that passed parameter.
  ///
  /// - parameter device: A device.
  ///
  /// - returns: the underlying device If the `device` is a `simulator`,
  /// otherwise return the `device`.
  public static func reaDevice(from device: Device) -> Device {
    
    if case let .simulator(model) = device {
      return model
    }
    
    return device
  }
  
  /// Returns diagonal screen length in inches
  public var diagonal: Double {
    
    switch self {
    case .iPodTouch5: return 4
    case .iPodTouch6: return 4
    case .iPhone4: return 3.5
    case .iPhone4s: return 3.5
    case .iPhone5: return 4
    case .iPhone5c: return 4
    case .iPhone5s: return 4
    case .iPhone6: return 4.7
    case .iPhone6Plus: return 5.5
    case .iPhone6s: return 4.7
    case .iPhone6sPlus: return 5.5
    case .iPhone7: return 4.7
    case .iPhone7Plus: return 5.5
    case .iPhoneSE: return 4
    case .iPhone8: return 4.7
    case .iPhone8Plus: return 5.5
    case .iPhoneX: return 5.8
    case .iPhoneXs: return 5.8
    case .iPhoneXsMax: return 6.5
    case .iPhoneXr: return 6.1
    case .iPhone11: return 6.1
    case .iPhone11Pro: return 5.8
    case .iPhone11ProMax: return 6.5
    case .iPad2: return 9.7
    case .iPad3: return 9.7
    case .iPad4: return 9.7
    case .iPadAir: return 9.7
    case .iPadAir2: return 9.7
    case .iPad5: return 9.7
    case .iPad6: return 9.7
    case .iPad7: return 10.2
    case .iPadMini: return 7.9
    case .iPadMini2: return 7.9
    case .iPadMini3: return 7.9
    case .iPadMini4: return 7.9
    case .iPadMini5: return 7.9
    case .iPadPro9Inch: return 9.7
    case .iPadPro12Inch: return 12.9
    case .iPadPro12Inch2: return 12.9
    case .iPadPro10Inch: return 10.5
    case .iPadPro11Inch: return 11.0
    case .iPadPro12Inch3: return 12.9
    case .simulator(let model): return model.diagonal
    case .unknown: return -1
    }
  }
  
  /// Returns screen ratio as a tuple
  public var screenRatio: (width: Double, height: Double) {
    
    switch self {
    case .iPodTouch5: return (width: 9, height: 16)
    case .iPodTouch6: return (width: 9, height: 16)
    case .iPhone4: return (width: 2, height: 3)
    case .iPhone4s: return (width: 2, height: 3)
    case .iPhone5: return (width: 9, height: 16)
    case .iPhone5c: return (width: 9, height: 16)
    case .iPhone5s: return (width: 9, height: 16)
    case .iPhone6: return (width: 9, height: 16)
    case .iPhone6Plus: return (width: 9, height: 16)
    case .iPhone6s: return (width: 9, height: 16)
    case .iPhone6sPlus: return (width: 9, height: 16)
    case .iPhone7: return (width: 9, height: 16)
    case .iPhone7Plus: return (width: 9, height: 16)
    case .iPhoneSE: return (width: 9, height: 16)
    case .iPhone8: return (width: 9, height: 16)
    case .iPhone8Plus: return (width: 9, height: 16)
    case .iPhoneX: return (width: 9, height: 19.5)
    case .iPhoneXs: return (width: 9, height: 19.5)
    case .iPhoneXsMax: return (width: 9, height: 19.5)
    case .iPhoneXr: return (width: 9, height: 19.5)
    case .iPhone11: return (width: 9, height: 19.5)
    case .iPhone11Pro: return (width: 9, height: 19.5)
    case .iPhone11ProMax: return (width: 9, height: 19.5)
    case .iPad2: return (width: 3, height: 4)
    case .iPad3: return (width: 3, height: 4)
    case .iPad4: return (width: 3, height: 4)
    case .iPadAir: return (width: 3, height: 4)
    case .iPadAir2: return (width: 3, height: 4)
    case .iPad5: return (width: 3, height: 4)
    case .iPad6: return (width: 3, height: 4)
    case .iPad7: return (width: 3, height: 4)
    case .iPadMini: return (width: 3, height: 4)
    case .iPadMini2: return (width: 3, height: 4)
    case .iPadMini3: return (width: 3, height: 4)
    case .iPadMini4: return (width: 3, height: 4)
    case .iPadMini5: return (width: 3, height: 4)
    case .iPadPro9Inch: return (width: 3, height: 4)
    case .iPadPro12Inch: return (width: 3, height: 4)
    case .iPadPro12Inch2: return (width: 3, height: 4)
    case .iPadPro10Inch: return (width: 3, height: 4)
    case .iPadPro11Inch: return (width: 139, height: 199)
    case .iPadPro12Inch3: return (width: 512, height: 683)
    case .simulator(let model): return model.screenRatio
    case .unknown: return (width: -1, height: -1)
    }
  }
  
  /// All iPods
  public static var allPods: [Device] {
    return [.iPodTouch5, .iPodTouch6]
  }
  
  /// All iPhones
  public static var allPhones: [Device] {
    return [.iPhone4, .iPhone4s, .iPhone5, .iPhone5c, .iPhone5s, .iPhone6, .iPhone6Plus, .iPhone6s, .iPhone6sPlus, .iPhone7, .iPhone7Plus, .iPhoneSE, .iPhone8, .iPhone8Plus, .iPhoneX, .iPhoneXs, .iPhoneXsMax, .iPhoneXr, .iPhone11, .iPhone11Pro, .iPhone11ProMax]
  }
  
  /// All iPads
  public static var allPads: [Device] {
    return [.iPad2, .iPad3, .iPad4, .iPadAir, .iPadAir2, .iPad5, .iPad6, .iPad7, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadPro9Inch, .iPadPro12Inch, .iPadPro12Inch2, .iPadPro10Inch, .iPadPro11Inch, .iPadPro12Inch3]
  }
  
  /// All X-Series Devices
  public static var allXSeriesDevices: [Device] {
    return [.iPhoneX, .iPhoneXs, .iPhoneXsMax, .iPhoneXr, .iPhone11, .iPhone11ProMax, .iPhone11Pro]
  }
  
  /// All Plus-Sized Devices
  public static var allPlusSizedDevices: [Device] {
    return [.iPhone6Plus, .iPhone6sPlus, .iPhone7Plus, .iPhone8Plus]
  }
  
  /// All Pro Devices
  public static var allProDevices: [Device] {
    return [.iPadPro9Inch, .iPadPro12Inch, .iPadPro12Inch2, .iPadPro10Inch, .iPadPro11Inch, .iPadPro12Inch3]
  }
  
  /// All mini Devices
  public static var allMiniDevices: [Device] {
    return [.iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5]
  }
  
  /// All simulator iPods
  public static var allSimulatorPods: [Device] {
    return allPods.map(Device.simulator)
  }
  
  /// All simulator iPhones
  public static var allSimulatorPhones: [Device] {
    return allPhones.map(Device.simulator)
  }
  
  /// All simulator iPads
  public static var allSimulatorPads: [Device] {
    return allPads.map(Device.simulator)
  }
  
  /// All simulator iPad mini
  public static var allSimulatorMiniDevices: [Device] {
    return allMiniDevices.map(Device.simulator)
  }
  
  /// All simulator Plus-Sized Devices
  public static var allSimulatorXSeriesDevices: [Device] {
    return allXSeriesDevices.map(Device.simulator)
  }
  
  /// All simulator Plus-Sized Devices
  public static var allSimulatorPlusSizedDevices: [Device] {
    return allPlusSizedDevices.map(Device.simulator)
  }
  
  /// All simulator Pro Devices
  public static var allSimulatorProDevices: [Device] {
    return allProDevices.map(Device.simulator)
  }
  
  /// Returns whether the device is an iPod (real or simulator)
  public static var isPod: Bool {
    return Device().isOneOf(Device.allPods) || Device().isOneOf(Device.allSimulatorPods)
  }
  
  /// Returns whether the device is an iPhone (real or simulator)
  public static var isPhone: Bool {
    return (Device().isOneOf(Device.allPhones)
      || Device().isOneOf(Device.allSimulatorPhones)
      || UIDevice.current.userInterfaceIdiom == .phone) && !isPod
  }
  
  /// Returns whether the device is an iPad (real or simulator)
  public static var isPad: Bool {
    return Device().isOneOf(Device.allPads)
      || Device().isOneOf(Device.allSimulatorPads)
      || UIDevice.current.userInterfaceIdiom == .pad
  }
  
  /// Returns whether the device is any of the simulator
  /// Useful when there is a need to check and skip running a portion of code (location request or others)
  public static var isSimulator: Bool {
    return Device().isOneOf(Device.allSimulators)
  }
  
  /// If this device is a simulator return the underlying device,
  /// otherwise return `self`.
  public var reaDevice: Device {
    return Device.reaDevice(from: self)
  }
  
  public var isZoomed: Bool {
    // TODO: Longterm we need a better solution for this!
    guard self != .iPhoneX && self != .iPhoneXs else { return false }
    if Int(UIScreen.main.scale.rounded()) == 3 {
      // Plus-sized
      return UIScreen.main.nativeScale > 2.7
    } else {
      return UIScreen.main.nativeScale > UIScreen.main.scale
    }
  }
  
  /// All Touch ID Capable Devices
  public static var allTouchIDCapableDevices: [Device] {
    return [.iPhone5s, .iPhone6, .iPhone6Plus, .iPhone6s, .iPhone6sPlus, .iPhone7, .iPhone7Plus, .iPhoneSE, .iPhone8, .iPhone8Plus, .iPadAir2, .iPad5, .iPad6, .iPadMini3, .iPadMini4, .iPadMini5, .iPadPro9Inch, .iPadPro12Inch, .iPadPro12Inch2, .iPadPro10Inch, .iPad7]
  }
  
  /// All Face ID Capable Devices
  public static var allFaceIDCapableDevices: [Device] {
    return [.iPhoneX, .iPhoneXs, .iPhoneXsMax, .iPhoneXr, .iPadPro11Inch, .iPadPro12Inch3, .iPhone11, .iPhone11Pro, .iPhone11ProMax]
  }
  
  /// Returns whether or not the device has Touch ID
  public var isTouchIDCapable: Bool {
    return isOneOf(Device.allTouchIDCapableDevices)
  }
  
  /// Returns whether or not the device has Face ID
  public var isFaceIDCapable: Bool {
    return isOneOf(Device.allFaceIDCapableDevices)
  }
  
  /// Returns whether or not the device has any biometric sensor (i.e. Touch ID or Face ID)
  public var hasBiometricSensor: Bool {
    return isTouchIDCapable || isFaceIDCapable
  }
  
  /// All real devices (i.e. all devices except for all simulators)
  public static var allReaDevices: [Device] {
    return allPods + allPhones + allPads
  }
  
  /// All simulators
  public static var allSimulators: [Device] {
    return allReaDevices.map(Device.simulator)
  }
  
  public static var all5SizeDevices: [Device] {
    return [.iPhone5s, .simulator(.iPhone5s), .iPhoneSE, .simulator(.iPhoneSE)]
  }
  
  public static var isiPhone5Size: Bool {
    return Device().isOneOf(Device.all5SizeDevices) || Device.screenHeight == 568.0
  }
  
  public static var isXDevice: Bool {
    return Device().isOneOf(allXSeriesDevices + allSimulatorXSeriesDevices)
  }
  
  public static var isPlusSizeDevice: Bool {
    return Device().isOneOf(allPlusSizedDevices + allSimulatorPlusSizedDevices)
  }
  /**
   This method saves you in many cases from the need of updating your code with every new device.
   Most uses for an enum like this are the following:
   
   - parameter devices: An array of devices.
   
   - returns: Returns whether the current device is one of the passed in ones.
   */
  public func isOneOf(_ devices: [Device]) -> Bool {
    return devices.contains(self)
  }
  
  /// The name identifying the device (e.g. "Dennis' iPhone").
  public var name: String {
    return UIDevice.current.name
  }
  
  /// The name of the operating system running on the device represented by the receiver (e.g. "iOS" or "tvOS").
  public var systemName: String {
    return UIDevice.current.systemName
  }
  
  /// The current version of the operating system (e.g. 8.4 or 9.2).
  public var systemVersion: String {
    return UIDevice.current.systemVersion
  }
  
  /// The model of the device (e.g. "iPhone" or "iPod Touch").
  public var model: String {
    return UIDevice.current.model
  }
  
  /// The model of the device as a localized string.
  public var localizedModel: String {
    return UIDevice.current.localizedModel
  }
  
  /// PPI (Pixels per Inch) on the current device's screen (if applicable). When the device is not applicable this property returns nil.
  public var ppi: Int? {

    switch self {
    case .iPodTouch5: return 326
    case .iPodTouch6: return 326
    case .iPhone4: return 326
    case .iPhone4s: return 326
    case .iPhone5: return 326
    case .iPhone5c: return 326
    case .iPhone5s: return 326
    case .iPhone6: return 326
    case .iPhone6Plus: return 401
    case .iPhone6s: return 326
    case .iPhone6sPlus: return 401
    case .iPhone7: return 326
    case .iPhone7Plus: return 401
    case .iPhoneSE: return 326
    case .iPhone8: return 326
    case .iPhone8Plus: return 401
    case .iPhoneX: return 458
    case .iPhoneXs: return 458
    case .iPhoneXsMax: return 458
    case .iPhoneXr: return 326
    case .iPhone11: return 326
    case .iPhone11Pro: return 458
    case .iPhone11ProMax: return 458
    case .iPad2: return 132
    case .iPad3: return 264
    case .iPad4: return 264
    case .iPadAir: return 264
    case .iPadAir2: return 264
    case .iPad5: return 264
    case .iPad6: return 264
    case .iPad7: return 264
    case .iPadMini: return 163
    case .iPadMini2: return 326
    case .iPadMini3: return 326
    case .iPadMini4: return 326
    case .iPadMini5: return 326
    case .iPadPro9Inch: return 264
    case .iPadPro12Inch: return 264
    case .iPadPro12Inch2: return 264
    case .iPadPro10Inch: return 264
    case .iPadPro11Inch: return 264
    case .iPadPro12Inch3: return 264
    case .simulator(let model): return model.ppi
    case .unknown: return nil
    }  }
  
  /// True when a Guided Access session is currently active; otherwise, false.
  public var isGuidedAccessSessionActive: Bool {
    
    #if swift(>=4.2)
    return UIAccessibility.isGuidedAccessEnabled
    #else
    return UIAccessibilityIsGuidedAccessEnabled()
    #endif
  }
  
  /// The brightness level of the screen.
  public var screenBrightness: Int {
    return Int(UIScreen.main.brightness * 100)
  }
}

// MARK: - CustomStringConvertible
extension Device: CustomStringConvertible {
  
  /// A textual representation of the device.
  public var description: String {

    switch self {
    case .iPodTouch5: return "iPod Touch 5"
    case .iPodTouch6: return "iPod Touch 6"
    case .iPhone4: return "iPhone 4"
    case .iPhone4s: return "iPhone 4s"
    case .iPhone5: return "iPhone 5"
    case .iPhone5c: return "iPhone 5c"
    case .iPhone5s: return "iPhone 5s"
    case .iPhone6: return "iPhone 6"
    case .iPhone6Plus: return "iPhone 6 Plus"
    case .iPhone6s: return "iPhone 6s"
    case .iPhone6sPlus: return "iPhone 6s Plus"
    case .iPhone7: return "iPhone 7"
    case .iPhone7Plus: return "iPhone 7 Plus"
    case .iPhoneSE: return "iPhone SE"
    case .iPhone8: return "iPhone 8"
    case .iPhone8Plus: return "iPhone 8 Plus"
    case .iPhoneX: return "iPhone X"
    case .iPhoneXs: return "iPhone Xs"
    case .iPhoneXsMax: return "iPhone Xs Max"
    case .iPhoneXr: return "iPhone Xr"
    case .iPhone11: return "iPhone 11"
    case .iPhone11Pro: return "iPhone 11 Pro"
    case .iPhone11ProMax: return "iPhone 11 Pro Max"
    case .iPad2: return "iPad 2"
    case .iPad3: return "iPad 3"
    case .iPad4: return "iPad 4"
    case .iPadAir: return "iPad Air"
    case .iPadAir2: return "iPad Air 2"
    case .iPad5: return "iPad 5"
    case .iPad6: return "iPad 6"
    case .iPad7: return "iPad 7"
    case .iPadMini: return "iPad Mini"
    case .iPadMini2: return "iPad Mini 2"
    case .iPadMini3: return "iPad Mini 3"
    case .iPadMini4: return "iPad Mini 4"
    case .iPadMini5: return "iPad Mini 5"
    case .iPadPro9Inch: return "iPad Pro (9.7-inch)"
    case .iPadPro12Inch: return "iPad Pro (12.9-inch)"
    case .iPadPro12Inch2: return "iPad Pro (12.9-inch) (2nd generation)"
    case .iPadPro10Inch: return "iPad Pro (10.5-inch)"
    case .iPadPro11Inch: return "iPad Pro (11-inch)"
    case .iPadPro12Inch3: return "iPad Pro (12.9-inch) (3rd generation)"
    case .simulator(let model): return "Simulator (\(model))"
    case .unknown(let identifier): return identifier
    }
  }
}

// MARK: - Equatable
extension Device: Equatable {
  
  /// Compares two devices
  ///
  /// - parameter lhs: A device.
  /// - parameter rhs: Another device.
  ///
  /// - returns: `true` iff the underlying identifier is the same.
  public static func == (lhs: Device, rhs: Device) -> Bool {
    return lhs.description == rhs.description
  }
  
}

// MARK: - Sizes
extension Device {

  public static var screenWidth: CGFloat {
    return UIScreen.main.bounds.size.width
  }
  
  public static var screenHeight: CGFloat {
    return UIScreen.main.bounds.size.height
  }
  
  public static var screenMaxLenght: CGFloat {
    return max(Device.screenWidth, Device.screenHeight)
  }
  
  public static var screenMinLenght: CGFloat {
    return min(Device.screenWidth, Device.screenHeight)
  }
}

// MARK: - Orientation
extension Device {
  
  public enum Orientation {
    case landscape, portrait
  }
  
  public static var orientation: Orientation {
    return UIDevice.current.orientation.isLandscape ? .landscape : .portrait
  }
  
  public static var isLandscape: Bool {
    return Device.orientation == .landscape
  }
  
  public static var isPortrait: Bool {
    return Device.orientation == .portrait
  }
  
  static func forceOrientation(_ orientation: UIInterfaceOrientation) {
    
    UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
    UIViewController.attemptRotationToDeviceOrientation()
  }
  
  static func forceLandscape() {
    
    if Device.isPortrait {
      UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
    }
  }
  
  static func forcePortrait() {
    
    if Device.isLandscape {
      UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    }
  }

}

extension Device {
  
  static let isBuddyfit = (Bundle.main.infoDictionary!["CFBundleName"] as! String).lowercased().contains("Buddyfit")
  
  static let isStaging = (Bundle.main.infoDictionary!["CFBundleName"] as! String).lowercased().contains("dev")
  
  static var version: String {
    return Bundle.main.infoDictionary!["CFBundleVersion"] as! String
  }
  
  static var iOS13: Bool {
    
    if #available(iOS 13, *) {
      return true
      
    } else {
      return false
    }
  }

  static var isDebugBuild: Bool {

    #if Development || Local
    return true
    #else
    return false
    #endif
  }
  
  static var isTestBuild: Bool {

    #if Test
    return true
    #else
    return false
    #endif
  }
  
  static var isLiveBuild: Bool {
    #if !Development && !Local && !Test
    return true
    #else
    return false
    #endif
  }

  
  static var build: String {
    let shortVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    let version = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
    return shortVersion + " rv:" + version
  }
  
  static var appName: String {
    
    var appName = Bundle.main.infoDictionary!["CFBundleDisplayName"] as? String
    if appName == nil {
      appName = Bundle.main.infoDictionary!["CFBundleName"] as? String
    }
    
    let latin1Data = appName?.data(using: String.Encoding.utf8)
    appName = NSString(data: latin1Data!, encoding: String.Encoding.isoLatin1.rawValue) as String?
    
    // If we couldn't find one, we'll give up (and AFNetwork will use the standard CFNetwork user agent)
    if appName == nil {
      appName = "Buddyfit";
    }
    
    return appName!
  }
}

