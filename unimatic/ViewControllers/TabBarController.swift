//
//  TabBarController.swift
//  unimatic
//
//  Created by Filippo Tosetto on 14/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit
import RealmSwift

class TabBarController: UITabBarController {
  
  private var locationManager: LocationManager!
  private var loaderView: LoaderView?
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  convenience init() {
    self.init(nibName: nil, bundle: nil)
    
    self.locationManager = LocationManager(delegate: self)

    self.tabBarController?.tabBar.isHidden = true
    
    self.viewControllers = [
      CurrentTimeViewController(headerDelegate: self),
      WorldClockViewController(headerDelegate: self),
      StopWatchViewController(headerDelegate: self)
    ]    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setNeedsStatusBarAppearanceUpdate()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    if Realm.getCurrentLocation() == nil, let window = self.view.window {
      self.loaderView = LoaderView.fromNib() as LoaderView
      self.loaderView?.configure(inView: window)
    }
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
}

extension TabBarController: HeaderViewDelegate {
  
  func willShowWorldClock() {
    self.selectedIndex = 1
  }
  
  func willShowCurrentTime() {
    self.selectedIndex = 0
  }
  
  func willShowStopWatch() {
    self.selectedIndex = 2
  }
}

// MARK: - LocationManagerDelegate

extension TabBarController: LocationManagerDelegate {
  
  func updatedCurrentLocation(withLocation location: RMLocation) {
    Realm.setCurrentLocation(location: location)
    self.loaderView?.dismiss()
  }
}
