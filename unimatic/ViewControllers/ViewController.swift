//
//  ViewController.swift
//  unimatic
//
//  Created by Filippo Tosetto on 29/04/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit
import MapKit

class MainViewController: UIViewController {

  private let locationManager = CLLocationManager()
  private let geocoder = CLGeocoder()

  @IBOutlet private weak var plusButton: UIButton!
  @IBOutlet private weak var clockFace: ClockFace!
  @IBOutlet private weak var currentCityLabel: UILabel!
  @IBOutlet private weak var timeLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.locationManager.delegate = self
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    self.locationManager.requestWhenInUseAuthorization()
    self.locationManager.requestLocation()
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  @IBAction func plusAction(_ sender: Any) {
  }
}

// MARK: - CLLocationManagerDelegate

extension MainViewController: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if status == .authorizedWhenInUse {
      self.locationManager.requestLocation()
    }
  }

  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else { return }
      
    self.geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
      guard let placemark = placemarks?.first else { return }
      
      self.update(withPlacemark: placemark)
      
      self.locationManager.stopUpdatingLocation()
    }
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {    
    print("error:: \(error)")
  }
}

extension MainViewController {
  
  private func update(withPlacemark placemark: CLPlacemark) {
    
    self.currentCityLabel.text = placemark.locality
    
    let format = DateFormatter()
    format.timeZone = placemark.timeZone!
    format.dateFormat = "HH:mm"
    self.timeLabel.text = format.string(from: Date())
    
    self.clockFace.drawClockHand(timezone: placemark.timeZone)
  }
  
}

