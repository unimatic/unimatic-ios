//
//  StopWatchViewController.swift
//  unimatic
//
//  Created by Filippo Tosetto on 14/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class StopWatchViewController: UIViewController {
  
  @IBOutlet private weak var headerViewContainer: UIView!
  
  @IBOutlet private weak var startButton: UIButton!
  @IBOutlet private weak var stopButton: UIButton!
  
  @IBOutlet private weak var stopWatchLabel: UILabel!
  @IBOutlet private weak var millisecondsLabel: UILabel!
  
  @IBOutlet private weak var tableView: UITableView! {
    didSet {
      self.tableView.dataSource = self
      
      self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)

      self.tableView.registerNibClass(StopWatchCell.self)
    }
  }
  @IBOutlet private weak var gradientView: UIView! {
    didSet {
      let colorTop = UIColor.clear.cgColor
      let colorBottom = UIColor.black.cgColor

      let gradient = CAGradientLayer()
      gradient.frame.size = self.gradientView.size
      gradient.frame.origin = CGPoint(x: 0.0, y: 0.0)

      gradient.colors = [colorTop, colorBottom]
      gradient.locations = [0.0, 1.0]
      
      gradient.startPoint = CGPoint(x: 0, y: 0)
      gradient.endPoint = CGPoint(x: 0, y: 1.0)

      self.gradientView.layer.insertSublayer(gradient, at: 0)
    }
  }
  
  private let headerView: HeaderView
  private let headerDelegate: HeaderViewDelegate
  
  private var currentLap = Lap(hours: 0, minutes: 0, seconds: 0, fractions: 0, counter: 0)
  private var runningLap = Lap(hours: 0, minutes: 0, seconds: 0, fractions: 0, counter: 0)
  private var lapsContainer = LapsContainer()
  
  private var timer = Timer()
  private var isPlaying = false
  // MARK: - Init
  
  init(headerDelegate: HeaderViewDelegate) {
    self.headerView = HeaderView.fromNib() as HeaderView
    self.headerDelegate = headerDelegate

    super.init(nibName: "StopWatchViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("not implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.tabBarController?.tabBar.isHidden = true
    
    self.headerView.configure(withInView: self.headerViewContainer, delegate: self.headerDelegate)
    self.setNeedsStatusBarAppearanceUpdate()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.headerView.selectStopWatch()
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  @IBAction func startAction(_ sender: Any) {
    
    // Play Action
    if !self.isPlaying {
      self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
      
      self.isPlaying = true
      
      self.stopButton.setTitle("STOP", for: .normal)
      self.startButton.setTitle("LAP", for: .normal)
      
    } else { // Lap Action
      self.lapsContainer.addNewLap(lap: self.currentLap)
      self.currentLap.setToZero()
      
      self.tableView.reloadData()
    }
  }
  
  @IBAction func resetAction(_ sender: Any) {

    // Stop action
    if self.isPlaying {
      self.timer.invalidate()
      self.isPlaying = false

      self.stopButton.setTitle("RESET", for: .normal)
      self.startButton.setTitle("START", for: .normal)
      
    } else { // Reset Action
      self.currentLap.setToZero()
      self.runningLap.setToZero()
      self.lapsContainer.cleanup()
      self.tableView.reloadData()
      
      self.stopWatchLabel.text = self.currentLap.getDescription()
      self.millisecondsLabel.text = ".\(self.currentLap.fractions)"
    }
  }
  
  @objc func updateTimer() {
    
    self.currentLap.update()
    self.runningLap.update()
    
    self.stopWatchLabel.text = self.runningLap.getDescription()
    self.millisecondsLabel.text = ".\(self.runningLap.fractions)"
  }
}

extension StopWatchViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.lapsContainer.count()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeReusableCellWithClass(StopWatchCell.self, forIndexPath: indexPath)!
    
    cell.configure(withLapViewModel: self.lapsContainer.object(atIndex: indexPath))
    return cell
  }
}
