//
//  CurrentTimeViewController.swift
//  unimatic
//
//  Created by Filippo Tosetto on 14/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit
import CoreLocation

class CurrentTimeViewController: UIViewController {

  enum Sections: CaseIterable {
    case currentTime, sunrise, moon, weather
    
    static func section(forIndexPath indexPath: IndexPath) -> Sections {
      switch indexPath.row {
      case 0:
        return .currentTime
        
      case 1:
        return .sunrise
        
      case 2:
        return .moon
        
      default:
        return .weather
      }
    }
  }

  @IBOutlet private weak var headerViewContainer: UIView!
  @IBOutlet private weak var tableView: UITableView! {
    didSet {
      self.tableView.dataSource = self
      self.tableView.delegate = self
      
      self.tableView.registerNibClass(CurrentTimeCell.self)
      self.tableView.registerNibClass(SunriseCell.self)
      self.tableView.registerNibClass(MoonCell.self)
      self.tableView.registerNibClass(WeatherListCell.self)
    }
  }
  
  private let headerView: HeaderView
  private let headerDelegate: HeaderViewDelegate
  
  private var currentLocation: String?
  private var weatherData: WeatherData?
  private let weatherManager = WeatherManager()

  private var locationQuery: RMQuery<RMLocation>?

  // MARK: - Init
  
  init(headerDelegate: HeaderViewDelegate) {
    self.headerView = HeaderView.fromNib() as HeaderView
    self.headerDelegate = headerDelegate
    
    super.init(nibName: "CurrentTimeViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("not implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.locationQuery = RMQuery.currentLocationQuery(delegate: self)
    self.tabBarController?.tabBar.isHidden = true
    
    self.headerView.configure(withInView: self.headerViewContainer, delegate: self.headerDelegate)
    self.setNeedsStatusBarAppearanceUpdate()    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.headerView.selectCurrentTime()
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  private func getWeather(forCity city: String) {
    
    self.weatherManager.weatherDataForLocation(city: city) { [weak self] data, _ in
      DispatchQueue.main.async {
        self?.weatherData = data
        self?.tableView.reloadData()
      }
    }
  }
}


// MARK: - UITableViewDataSource

extension CurrentTimeViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Sections.allCases.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch Sections.section(forIndexPath: indexPath) {
    case .currentTime:
      let cell = tableView.dequeReusableCellWithClass(CurrentTimeCell.self, forIndexPath: indexPath)!

      cell.configure(withLocation: self.currentLocation ?? "")
      return cell

    case .sunrise:
      let cell = tableView.dequeReusableCellWithClass(SunriseCell.self, forIndexPath: indexPath)!

      if let weatherData = self.weatherData {
        cell.configure(with: weatherData)
      }
      
      return cell
      
    case .moon:
      let cell = tableView.dequeReusableCellWithClass(MoonCell.self, forIndexPath: indexPath)!

      cell.configure()
      return cell
      
    default:
      let cell = tableView.dequeReusableCellWithClass(WeatherListCell.self, forIndexPath: indexPath)!
      
      if let data = self.weatherData {
        cell.configure(with: data)
      }
      
      return cell
    }
  }
}

// MARK: - UITableViewDelegate
extension CurrentTimeViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    switch Sections.section(forIndexPath: indexPath) {
    case .currentTime:
      return 230
      
    case .sunrise:
      return SunriseCell.height
      
    case .moon:
      return MoonCell.height
      
    default:
      return WeatherListCell.height
    }
  }
}

// MARK: - LocationManagerDelegate

extension CurrentTimeViewController: RMQueryDelegate {
  
  func realmQueryDidChange(_ changes: RMQueryChanges, query: AnyObject) {
    guard let location = self.locationQuery?.results.first else {
      return
    }
    
    self.currentLocation = location.name
    
    self.getWeather(forCity: location.name)
    self.tableView.reloadData()
  }
}
