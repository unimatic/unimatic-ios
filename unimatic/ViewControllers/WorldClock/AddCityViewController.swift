//
//  AddCityViewController.swift
//  unimatic
//
//  Created by Filippo Tosetto on 29/04/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class AddCityViewController: UIViewController {

  @IBOutlet private weak var tableView: UITableView! {
    didSet {
      self.tableView.dataSource = self
      self.tableView.delegate = self
      self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
  }
  
  private let searchController = UISearchController(searchResultsController: nil)
  private let searchCompleter = MKLocalSearchCompleter()
  private var searchResults = [MKLocalSearchCompletion]() {
    didSet {
      self.tableView.reloadData()
    }
  }
  
  private var locations = [RMLocation]()

  init() {
    super.init(nibName: "AddCityViewController", bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.searchCompleter.delegate = self
    self.searchCompleter.resultTypes = .address
    
    self.searchController.searchResultsUpdater = self
    self.searchController.searchBar.barStyle = .black
    self.searchController.searchBar.tintColor = .white
    self.definesPresentationContext = true
    self.tableView.tableHeaderView = self.searchController.searchBar
    
    self.locations = Realm.getOtherLocations()
  }
}

// MARK: - UITableViewDataSource

extension AddCityViewController: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return (section == 0) ? self.locations.count : self.searchResults.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    
    cell.backgroundColor = UIColor.clear
    cell.textLabel?.textColor = UIColor.white
    cell.detailTextLabel?.textColor = UIColor.white
    
    if indexPath.section == 0 {
      let location = self.locations[indexPath.row]
      cell.textLabel?.text = location.name
      
    } else {
      let searchResult = self.searchResults[indexPath.row]
      cell.textLabel?.attributedText = self.highlightedText(searchResult.title, inRanges: searchResult.titleHighlightRanges, size: 17.0)
      cell.detailTextLabel?.attributedText = self.highlightedText(searchResult.subtitle, inRanges: searchResult.subtitleHighlightRanges, size: 12.0)
    }

    return cell
  }
}

// MARK: - UITableViewDelegate

extension AddCityViewController: UITableViewDelegate {
    
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return section == 0 ? "Current Selected" : "Search"
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return indexPath.section == 0
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    
    Realm.deleteLocation(location: self.locations[indexPath.row])
    
    self.locations.remove(at: indexPath.row)
    self.tableView.deleteRows(at: [indexPath], with: .fade)
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard indexPath.section > 0 else {
      return
    }
      
    let completion = self.searchResults[indexPath.row]
      
    let searchRequest = MKLocalSearch.Request(completion: completion)
    let search = MKLocalSearch(request: searchRequest)
    search.start { [weak self] (response, error) in
      
      guard let resp = response else {
        return
      }
      
      if let item = resp.mapItems.first {
        Realm.addNewLocation(withName: completion.title, andPlacemark: item.placemark, timezone: item.timeZone)
        
        self?.dismiss(animated: true, completion: nil)
      }
    }
  }
}

// MARK: - UISearchResultsUpdating method

extension AddCityViewController: UISearchResultsUpdating {
  
  func updateSearchResults(for searchController: UISearchController) {
    guard let searchText = searchController.searchBar.text, searchText.count >= 3 else {
      return
    }
    
    self.searchCompleter.queryFragment = searchText
  }
}

// MARK: - MKLocalSearchCompleterDelegate

extension AddCityViewController: MKLocalSearchCompleterDelegate {
  
  func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
    self.searchResults = completer.results
  }
}

// MARK: - Private

extension AddCityViewController {
  
  private func highlightedText(_ text: String, inRanges ranges: [NSValue], size: CGFloat) -> NSAttributedString {
    
    let attributedText = NSMutableAttributedString(string: text)
    let regular = UIFont.systemFont(ofSize: size)
    
    attributedText.addAttribute(NSAttributedString.Key.font, value:regular, range:NSMakeRange(0, text.count))

    let bold = UIFont.boldSystemFont(ofSize: size)
    for value in ranges {
      attributedText.addAttribute(NSAttributedString.Key.font, value:bold, range:value.rangeValue)
    }
    return attributedText
  }
}

