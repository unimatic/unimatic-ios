//
//  WorldClockViewController.swift
//  unimatic
//
//  Created by Filippo Tosetto on 14/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit
import MapKit
import EventKitUI
import RealmSwift

class WorldClockViewController: UIViewController {

  @IBOutlet weak var headerViewContainer: UIView!
  @IBOutlet private weak var clockFace: ClockFace!
  @IBOutlet private weak var topView: UIView!
  @IBOutlet private weak var dateLabel: UILabel!
  
  @IBOutlet private weak var dateDifferenceLabel: UILabel!

  private let headerView: HeaderView
  private let headerDelegate: HeaderViewDelegate
  private let wordClockHeaderView: WorldClockHeader
  
  private var currentLocation: RMLocation? 
  private var selectedLocation: RMLocation? {
    didSet {
      self.drawClockHands()
    }
  }
  private var locations = [RMLocation]()
  private let currentDate = Date()
  private var newDate = Date() {
    didSet {
      self.updateLabels()
    }
  }

  private let eventStore = EKEventStore()
  private let currentDateFormatter = DateFormatter()
  private var locationQuery: RMQuery<RMLocation>?

  // MARK: - Init
  
  init(headerDelegate: HeaderViewDelegate) {
    self.headerView = HeaderView.fromNib() as HeaderView
    self.headerDelegate = headerDelegate

    self.wordClockHeaderView = WorldClockHeader.fromNib() as WorldClockHeader
    super.init(nibName: "WorldClockViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("not implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.tabBarController?.tabBar.isHidden = true
    self.locationQuery = RMQuery.locationsQuery(delegate: self)

    self.clockFace.delegate = self

    self.headerView.configure(withInView: self.headerViewContainer, delegate: self.headerDelegate)
    self.wordClockHeaderView.configure(inView: self.topView, withDelegate: self)
    
    self.setNeedsStatusBarAppearanceUpdate()
    
    self.currentDateFormatter.dateFormat = "d MMM"
    self.dateLabel.text = self.currentDateFormatter.string(from: self.currentDate)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    self.headerView.selectWorldClock()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  
    self.drawClockHands()
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  // MARK: - IBActions
  
  @IBAction func shareAction(_ sender: Any) {
  }
  
  @IBAction func calendarAction(_ sender: Any) {
    
    switch EKEventStore.authorizationStatus(for: .event) {
      case .notDetermined:
        self.eventStore.requestAccess(to: .event) { granted, _ in
          guard granted else { return }
          
          DispatchQueue.main.async {
            self.showEventViewController()
          }
        }
      
      case .authorized:
        DispatchQueue.main.async {
          self.showEventViewController()
        }

      default:
          break
    }
  }
  
  @IBAction func nextDateAction(_ sender: Any) {
    self.newDate = self.newDate.dateByAddingDays(1)!
  }
  
  @IBAction func previousDateAction(_ sender: Any) {
    self.newDate = self.newDate.dateByAddingDays(-1)!
  }
  
  func showEventViewController() {
    
    let event = EKEvent(eventStore: self.eventStore)
    event.startDate = self.newDate
    
    let eventVC = EKEventEditViewController()
    eventVC.editViewDelegate = self
    eventVC.eventStore = self.eventStore
    eventVC.event = event

    self.present(eventVC, animated: true)
  }
}

// MARK: - EKEventEditViewDelegate

extension WorldClockViewController: EKEventEditViewDelegate {
  
  func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
    self.dismiss(animated: true, completion: nil)
  }
}

// MARK: - WorldClockHeaderDelegate

extension WorldClockViewController: WorldClockHeaderDelegate {
  
  func didScrollToPosition(position: Int) {
    self.selectedLocation = self.locations[position]
  }

  func willAddNewCity() {
    let addCityViewController = AddCityViewController()
    let navController = UINavigationController(rootViewController: addCityViewController)
    navController.isNavigationBarHidden = true
    self.present(navController, animated: true, completion: nil)
  }
  
  func willReset() {
    self.newDate = self.currentDate
    self.clockFace.rotateBack()
  }

  private func drawClockHands() {
  
    self.clockFace.removeAllHands()
    
    for location in self.locations {
      self.clockFace.drawClockHand(forLocation: location, selected: self.selectedLocation == location)
    }
  }
}

// MARK: - ClockFaceDelegate

extension WorldClockViewController: ClockFaceDelegate {
  
  func didRotate(toAngle angle: CGFloat) {
    let minutes = angle * (24 / 360) * 60
    
    self.newDate = self.currentDate.dateByAddingHinutes(Int(minutes))!
  }
}

// MARK: - Private

extension WorldClockViewController {
 
  private func updateLabels() {
    self.wordClockHeaderView.updateDateLabel(withDate: self.newDate)
    self.dateLabel.text = self.currentDateFormatter.string(from: self.newDate)
    self.dateDifferenceLabel.text = self.newDate.offset(from: self.currentDate)
  }
}

// MARK: - RMQueryDelegate

extension WorldClockViewController: RMQueryDelegate {
  
  func realmQueryDidChange(_ changes: RMQueryChanges, query: AnyObject) {
    guard let locations = self.locationQuery?.results else {
      return
    }
    self.locations = Array(locations)

    self.currentLocation = locations.first(where: { $0.current == true })
    self.selectedLocation = self.currentLocation
    
    self.drawClockHands()
  }
}
