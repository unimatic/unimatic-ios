//
//  WeatherManager.swift
//  unimatic
//
//  Created by Filippo Tosetto on 27/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

enum WeatherManagerError: Error {

  case Unknown
  case FailedRequest
  case InvalidResponse
}


class WeatherManager {

  typealias WeatherDataCompletion = (WeatherData?, WeatherManagerError?) -> ()

  private let APIKey = "271537219331766fbdaf30a4ef37fb33"
  private let BaseURL = "https://api.openweathermap.org/data/2.5/forecast/daily?q=%@&appid=%@&units=metric"

  // MARK: - Initialization

  func weatherDataForLocation(city: String, completion: @escaping WeatherDataCompletion) {
    
    let urlPath = URL(string: String(format: self.BaseURL, city.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, self.APIKey))!

    URLSession.shared.dataTask(with: urlPath) { (data, response, error) in
      self.didFetchWeatherData(data: data, response: response, error: error, completion: completion)
    }.resume()
  }

  // MARK: - Helper Methods

  private func didFetchWeatherData(data: Data?, response: URLResponse?, error: Error?, completion: WeatherDataCompletion) {
    if let _ = error {
      completion(nil, .FailedRequest)

    } else if let data = data, let response = response as? HTTPURLResponse {
      if response.statusCode == 200 {
        if let weatherData = self.processWeatherData(data: data) {
          completion(weatherData, nil)
        } else {
          completion(nil, .InvalidResponse)
        }
        
      } else {
        completion(nil, .FailedRequest)
      }

    } else {
      completion(nil, .Unknown)
    }
  }
  
  private func processWeatherData(data: Data) -> WeatherData? {
    return try? JSONDecoder().decode(WeatherData.self, from: data)
  }
}
