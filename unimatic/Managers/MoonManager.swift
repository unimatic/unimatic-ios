//
//  MoonManager.swift
//  unimatic
//
//  Created by Filippo Tosetto on 27/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct MoonManager {
  
  enum MoonPhase {
    case newMoon, firstQuarter, fullMoon, lastQuarter
    
    static func getPhase(fromValue value: Double) -> MoonPhase {
      switch value {
      case 0..<0.25:
        return .newMoon
        
      case 0.25..<0.5:
        return .firstQuarter
        
      case 0.5..<0.75:
        return .lastQuarter
        
      default:
        return .fullMoon
      }
    }
  }
  
  static func getCurrentMoonPhase() -> (MoonPhase, Double?) {
    let suncalc = SwiftySuncalc()
    let moon = suncalc.getMoonIllumination(date: Date())
    let phase = moon["phase"] ?? 0.0
    return (MoonPhase.getPhase(fromValue: phase), Double(round(10*phase*100)/10))
  }

}
