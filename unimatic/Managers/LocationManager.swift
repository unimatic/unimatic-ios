//
//  LocationManager.swift
//  unimatic
//
//  Created by Filippo Tosetto on 24/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate: class {
  func updatedCurrentLocation(withLocation location: RMLocation)
}

extension LocationManagerDelegate {
  func updatedCurrentCity(withCity city: String) {}
  func updatedCurrentPlacemark(withPlacemark placemark: CLPlacemark) {}
}

class LocationManager: NSObject {
    
  private let locationManager = CLLocationManager()
  private let geocoder = CLGeocoder()
  private var currentLocation: RMLocation? {
    didSet {
      if let location = self.currentLocation {
        self.delegate?.updatedCurrentLocation(withLocation: location)
      }
    }
  }
  
  private weak var delegate: LocationManagerDelegate?
  
  init(delegate: LocationManagerDelegate?) {
    self.delegate = delegate

    super.init()
    
    self.locationManager.delegate = self
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    self.locationManager.requestWhenInUseAuthorization()
    self.locationManager.requestLocation()
  }
}


// MARK: - Core Location Delegate
extension LocationManager: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if status == .authorizedWhenInUse {
      self.locationManager.requestLocation()
    }
  }

  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else { return }
      
    self.geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
      guard let placemark = placemarks?.first else { return }
      
      self.currentLocation = RMLocation(placemark: placemark)
      self.locationManager.stopUpdatingLocation()
    }
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) { }
}

extension LocationManager {
  
  private func update(withPlacemark placemark: CLPlacemark) -> String {
    return placemark.locality ?? placemark.name ?? "unknown"
  }
}
