//
//  StopWatchCell.swift
//  unimatic
//
//  Created by Filippo Tosetto on 30/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class StopWatchCell: UITableViewCell {
    
  @IBOutlet private weak var timeLabel: UILabel!
  @IBOutlet private weak var lapNumberLabel: UILabel!
  @IBOutlet private weak var differenceLabel: UILabel!
  @IBOutlet private weak var starIcon: UIImageView!
  
  func configure(withLapViewModel lapViewModel: LapViewModel) {
    self.timeLabel.text = lapViewModel.lap.getFullDescription()
    
    self.lapNumberLabel.text = "Lap \(lapViewModel.lapNumber)"
    self.differenceLabel.text = lapViewModel.getDifferenceDescription()
    
    self.differenceLabel.isHidden = lapViewModel.isBest
    self.starIcon.isHidden = !lapViewModel.isBest
  }
}
