//
//  CurrentTimeCell.swift
//  unimatic
//
//  Created by Filippo Tosetto on 24/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class CurrentTimeCell: UITableViewCell {

  @IBOutlet weak var currentTimeLabel: UILabel!
  @IBOutlet weak var millisecondLable: UILabel!
  @IBOutlet weak var locationLabel: UILabel!
  @IBOutlet weak var weekdayLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  
  private var minuteTimer: Timer?
  private var secondsTimer: Timer?

  private var timeFormatter: DateFormatter {
    
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    
    return formatter
  }
  
  private var millisecondsFormatter: DateFormatter {
    
    let formatter = DateFormatter()
    formatter.dateFormat = "ss.S"
    
    return formatter
  }
  
  private var weeekdayFormatter: DateFormatter {
    
    let formatter = DateFormatter()
    formatter.dateFormat = "EEEE"
    
    return formatter
  }
  
  private var dateFormatter: DateFormatter {
    
    let formatter = DateFormatter()
    formatter.dateFormat = "d MMM yyyy"
    
    return formatter
  }
  
  private let currentDate = Date()
  
  func configure(withLocation location: String) {
    self.locationLabel.text = "\(location), \(self.hoursFromGMT())"

    self.weekdayLabel.text = self.weeekdayFormatter.string(from: self.currentDate)
    self.dateLabel.text = self.dateFormatter.string(from: self.currentDate)
    
    self.setupTimers()
  }
  
  private func setupTimers() {
    self.minuteTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
      self.currentTimeLabel.text = self.getCurrentTime()
    }
    
    self.secondsTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
      self.millisecondLable.text = self.getMilliseconds()
    }
  }
  
  private func getMilliseconds() -> String {
    let date = Date()
    return self.millisecondsFormatter.string(from: date)
  }
  
  private func getCurrentTime() -> String{
    let date = Date()
    return self.timeFormatter.string(from: date)
  }
  
  private func hoursFromGMT() -> String {
    let hours = TimeZone.current.secondsFromGMT() / 3600
    return "UTC/GMT " + (hours > 0 ? "+\(hours)" : "-\(hours)") + (abs(hours) > 1 ? " Hours" : " Hour")
  }
}
