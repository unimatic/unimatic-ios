//
//  SunriseCell.swift
//  unimatic
//
//  Created by Filippo Tosetto on 27/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class SunriseCell: UITableViewCell {

  @IBOutlet private weak var sunriseLabel: UILabel!
  @IBOutlet private weak var sunsetLabel: UILabel!
  @IBOutlet private weak var totalDaylightLabel: UILabel!
  
  static let height: CGFloat = 125
  
  func configure(with weather: WeatherData) {
    
    guard let dailyWeather = weather.list.first else {
      return
    }
    
    let sunrise = Date(timeIntervalSince1970: TimeInterval(dailyWeather.sunrise))
    let sunset = Date(timeIntervalSince1970: TimeInterval(dailyWeather.sunset))
    
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(secondsFromGMT: weather.city.timezone)
    dateFormatter.dateFormat = "HH:mm"
    
    self.sunriseLabel.text = dateFormatter.string(from: sunrise)
    self.sunsetLabel.text = dateFormatter.string(from: sunset)
      
    let components = Calendar.current.dateComponents([.hour, .minute], from: sunrise, to: sunset)
    self.totalDaylightLabel.text = "\(components.hour ?? 0):\(components.minute ?? 0)"
  }
}
