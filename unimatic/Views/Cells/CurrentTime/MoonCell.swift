//
//  MoonCell.swift
//  unimatic
//
//  Created by Filippo Tosetto on 27/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class MoonCell: UITableViewCell {

  @IBOutlet private weak var utcLabel: UILabel!
  @IBOutlet private weak var yearLabel: UILabel!
  @IBOutlet private weak var moonLabel: UILabel!
  @IBOutlet private weak var moonIconView: UIView!
  
  static let height: CGFloat = 125
  
  func configure() {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    self.utcLabel.text = dateFormatter.string(from: Date())

    let yearComponent = Calendar.current.component(.year, from: Date())
    self.yearLabel.text = "\(yearComponent)"
    
    let moonPhase = MoonManager.getCurrentMoonPhase()
    self.moonLabel.text = "\(moonPhase.1 ?? 0.0)%"
    
    self.setupMoon(withMoonPhase: moonPhase.0)
  }
  
  
  private func setupMoon(withMoonPhase moonPhase: MoonManager.MoonPhase) {
    self.moonIconView.layer.cornerRadius = self.moonIconView.height / 2
    self.moonIconView.layer.borderWidth = 2.0
    self.moonIconView.layer.borderColor = Constants.Colors.primary.cgColor
    
    
    switch moonPhase {
    case .firstQuarter, .lastQuarter:
      self.addHalfMoon()
      
    case .fullMoon:
      self.moonIconView.backgroundColor = Constants.Colors.primary
      
    case .newMoon:
      break
      
    }
  }
  
  private func addHalfMoon() {
    let size = self.moonIconView.bounds.width
    let shape = CAShapeLayer()
    shape.frame = self.moonIconView.bounds
    shape.fillColor = Constants.Colors.primary.cgColor
    let path = UIBezierPath(arcCenter: CGPoint(x: size/2, y: size/2),
                            radius: size/2,
                            startAngle: 3*CGFloat.pi/2,
                            endAngle: CGFloat.pi/2,
                            clockwise: false)
    shape.path = path.cgPath
    
    self.moonIconView.layer.addSublayer(shape)

  }
}
