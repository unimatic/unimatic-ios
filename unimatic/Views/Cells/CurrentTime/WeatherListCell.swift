//
//  WeatherListCell.swift
//  unimatic
//
//  Created by Filippo Tosetto on 27/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class WeatherListCell: UITableViewCell {

  @IBOutlet weak var collectionView: UICollectionView! {
    didSet {
      self.collectionView.dataSource = self
      self.collectionView.delegate = self
      
      self.collectionView.registerNibClass(WeatherCell.self)
      
      let layout = UICollectionViewFlowLayout()
      layout.scrollDirection = .horizontal
      layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
      self.collectionView.collectionViewLayout = layout
    }
  }
  
  static let height: CGFloat = 240
  private var weatherData: WeatherData?
  
  func configure(with weatherData: WeatherData) {
    self.weatherData = weatherData
    
    self.collectionView.reloadData()
  }
}

// MARK: - UICollectionViewDataSource

extension WeatherListCell: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.weatherData?.list.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

    let cell = collectionView.dequeReusableCellWithClass(WeatherCell.self, forIndexPath: indexPath)!
    if let weather = self.weatherData?.list[indexPath.row] {
      cell.configure(with: weather)
    }
    return cell
  }
}

extension WeatherListCell: UICollectionViewDelegate {}

// MARK: - UICollectionViewDelegateFlowLayout

extension WeatherListCell: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return WeatherCell.size
  }
}
