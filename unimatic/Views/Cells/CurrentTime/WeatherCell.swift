//
//  WeatherCell.swift
//  unimatic
//
//  Created by Filippo Tosetto on 27/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class WeatherCell: UICollectionViewCell {

  @IBOutlet private weak var weekDayLabel: UILabel!
  @IBOutlet private weak var weatherIcon: UIImageView!
  @IBOutlet private weak var maxTempLabel: UILabel!
  @IBOutlet private weak var minTempLabel: UILabel!
  
  static let size = CGSize(width: 54, height: 145)
    
  func configure(with dailyWeather: DailyWeather) {
    
    let date = Date(timeIntervalSince1970: TimeInterval(dailyWeather.dt))
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "EEE"
    self.weekDayLabel.text = dateFormatter.string(from: date).uppercased()
    
    self.maxTempLabel.text = "\(Int(dailyWeather.temp.max)) °"
    self.minTempLabel.text = "\(Int(dailyWeather.temp.min)) °"

    if let weather = dailyWeather.weather.first, let symbol = WeatherIcons(value: weather.icon)?.systemSymbol {
      self.weatherIcon.image = UIImage(systemName: symbol)
    }
  }
}
