//
//  ClockFace.swift
//  unimatic
//
//  Created by Filippo Tosetto on 30/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

protocol ClockFaceDelegate: class {
  func didRotate(toAngle angle: CGFloat)
}

class ClockFace: UIView {

  private var handsLayer: CALayer!
  private var hands = [String: CALayer]()
  
  private var sunrise: CAShapeLayer?
  private var sunset: CAShapeLayer?
  private var imagesLayer: CALayer?
  
  weak var delegate: ClockFaceDelegate?
  
  private var startTransform: CATransform3D = CATransform3DIdentity
  private var midPoint: CGPoint = .zero
  private var innerRadius: CGFloat = 0.0
  private var outerRadius: CGFloat = 0.0
  private var cumulatedAngle: CGFloat = 0.0

  override public func draw(_ rect: CGRect) {
    
    let radius = (self.width - 50)/2

    self.drawPoints(radius: radius-7, count: 24)
    self.drawNumbers(radius: self.width/2)
        
    self.handsLayer = CALayer()
    self.handsLayer.frame = rect
    self.layer.addSublayer(self.handsLayer)

    self.midPoint = CGPoint(x: self.frame.midX, y: self.frame.height/2)
    self.outerRadius = self.frame.size.width / 2
    self.innerRadius = self.outerRadius / 3
  }
  
  func drawSun(forLocation location: RMLocation) {
    
    let solarData = location.getSolar(forDate: Date())
    
    let center = CGPoint(x: self.width / 2, y: self.height / 2)
    let radius = (self.width - 50)/2

    if self.sunrise != nil {
      self.sunrise?.removeFromSuperlayer()
    }
    
    if self.sunset != nil {
      self.sunset?.removeFromSuperlayer()
    }

    func angle(forHours hours: CGFloat, andMinutes minutes: CGFloat) -> CGFloat {
      return CGFloat(hours * (360 / 24)) + CGFloat(minutes) * (1.0 / 60) * (360 / 24)
    }

    var calendar = Calendar.current
    if let timezone = location.timezone {
      calendar.timeZone = timezone
    }
    let sunriseHour = CGFloat(calendar.component(.hour, from: solarData.sunrise ?? Date()))
    let sunriseMinutes = CGFloat(calendar.component(.minute, from: solarData.sunrise ?? Date()))

    let sunsetHour = CGFloat(calendar.component(.hour, from: solarData.sunset ?? Date()))
    let sunsetMinutes = CGFloat(calendar.component(.minute, from: solarData.sunset ?? Date()))
    
    self.sunrise = self.addHalfCircle(center: center,
                                      radius: radius,
                                      startAngle: angle(forHours: sunriseHour, andMinutes: sunriseMinutes),
                                      endAngle: angle(forHours: sunsetHour, andMinutes: sunsetMinutes),
                                      fillColor: UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0),
                                      top: true)
    
    self.sunset = self.addHalfCircle(center: center,
                                     radius: radius,
                                     startAngle: angle(forHours: sunsetHour, andMinutes: sunsetMinutes),
                                     endAngle: angle(forHours: sunriseHour, andMinutes: sunriseMinutes),
                                     fillColor: UIColor(red: 25.0/255.0, green: 25.0/255.0, blue: 25.0/255.0, alpha: 1.0),
                                     top: false)
    
    if self.imagesLayer != nil {
      self.imagesLayer?.removeFromSuperlayer()
    }
    
    self.imagesLayer = CALayer()
    self.imagesLayer?.frame = self.bounds
    
    let moonLayer = CALayer()
    let moonImage = UIImage(systemName: "moon.fill")
    moonImage?.withTintColor(UIColor(red: 25.0/255.0, green: 25.0/255.0, blue: 25.0/255.0, alpha: 1.0))
    moonLayer.frame = CGRect(x: (self.width - 25)/2, y: self.height - 70 - 25, width: 25.0, height: 25.0)
    moonLayer.contents = moonImage?.cgImage
    self.imagesLayer?.addSublayer(moonLayer)

    let sunLayer = CALayer()
    let sunImage = UIImage(systemName: "sun.min")
    sunImage?.withTintColor(UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0))
    sunLayer.frame = CGRect(x: (self.width - 25)/2, y: 70, width: 25, height: 25)
    sunLayer.contents = sunImage?.cgImage
    self.imagesLayer?.addSublayer(sunLayer)
    
    self.layer.insertSublayer(self.imagesLayer!, at: 2)
  }
  
  func drawClockHand(forLocation location: RMLocation, selected: Bool = false){
    guard self.handsLayer != nil else {
      return
    }
    
    if selected {
      self.drawSun(forLocation: location)
    }
    
    var cityName = String(location.name.split(separator: ",").first ?? "")
    if cityName.isEmpty {
      cityName = location.name
    }
  
    let rect = self.bounds
    
    let hourHandLayer = CALayer()
    hourHandLayer.backgroundColor = selected ? Constants.Colors.primary.cgColor : UIColor.gray.cgColor 
    
    // Puts the center of the rectangle in the center of the clock
    hourHandLayer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
    // Positions the hand in the middle of the clock
    hourHandLayer.position = CGPoint(x: rect.size.width / 2, y: rect.size.height / 2)
    hourHandLayer.bounds = CGRect(x: 0, y: rect.size.height, width: 2, height: (rect.size.width / 2) - (rect.size.width * 0.07))
    hourHandLayer.contentsScale = UIScreen.main.scale

    let textlayer = CATextLayer()
    textlayer.anchorPoint = .zero
    textlayer.position = CGPoint(x: -rect.size.width / 2, y: -rect.size.height / 2)
    textlayer.frame = CGRect(x: 0, y: rect.size.height, width: (rect.size.width / 2) - (rect.size.width * 0.07), height: 20)
    textlayer.fontSize = 12
    textlayer.alignmentMode = .center
    textlayer.string = cityName.uppercased()
    textlayer.isWrapped = true
    textlayer.truncationMode = .end
    textlayer.backgroundColor = UIColor.clear.cgColor
    textlayer.foregroundColor = selected ? Constants.Colors.primary.cgColor : UIColor.white.cgColor
    textlayer.transform = CATransform3DMakeRotation(CGFloat(90).radians(), 0, 0, 1)
    textlayer.contentsScale = UIScreen.main.scale

    hourHandLayer.addSublayer(textlayer)
    
    self.handsLayer.addSublayer(hourHandLayer)
    
    let date = Date()
    var calendar = Calendar.current
    if let timezone = location.timezone {
      calendar.timeZone = timezone
    }
    let hours = calendar.component(.hour, from: date)
    let minutes = calendar.component(.minute, from: date)
    
    let hourAngle = CGFloat(hours * (360 / 24)) + CGFloat(minutes) * (1.0 / 60) * (360 / 24)
    hourHandLayer.transform = CATransform3DMakeRotation(hourAngle.radians(), 0, 0, 1)

    self.hands[location.name] = hourHandLayer
  }

  func removeAllHands() {
    
    for layer in self.hands.values {
      layer.removeFromSuperlayer()
    }
    
    self.hands.removeAll()
  }

  func removeHand(forCity city: String) {
    
    if let layer = self.hands[city] {
      layer.removeFromSuperlayer()
    }
    
    self.hands.removeValue(forKey: city)
  }
}

extension ClockFace {
  
   override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

     guard touches.count == 1 else { return }
     
     let nowPoint = touches.first!.location(in: self)
     let prevPoint = touches.first!.previousLocation(in: self)
     
        // make sure the new point is within the area
     let distance = CGPoint.distanceBetweenPoints(point1: self.midPoint, point2: nowPoint)
     if self.innerRadius <= distance && distance <= self.outerRadius {

       // calculate rotation angle between two points
       var angle = CGPoint.angleBetweenLinesInDegrees(beginLineA: self.midPoint,
                                                      endLineA: prevPoint,
                                                      beginLineB: self.midPoint,
                                                      endLineB: nowPoint)
     
       // fix value, if the 12 o'clock position is between prevPoint and nowPoint
       if (angle > 180) {
         angle -= 360
       } else if (angle < -180) {
         angle += 360
       }
       self.rotation(angle: angle)
     }
   }
   
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.cumulatedAngle = 0
  }
   
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.cumulatedAngle = 0
  }
   
   func rotation(angle: CGFloat) {
     
     self.cumulatedAngle += angle
         
     let rotation = CATransform3DMakeRotation(self.cumulatedAngle * .pi / 180, 0.0, 0.0, 1.0)
     self.handsLayer.transform = CATransform3DConcat(self.startTransform, rotation)
     
     let a = atan2(self.handsLayer.transform.m12, self.handsLayer.transform.m11)
     self.delegate?.didRotate(toAngle: a.degrees())
   }
   
   func rotateBack() {
     self.handsLayer.transform = CATransform3DIdentity
     
     let a = atan2(self.handsLayer.transform.m12, self.handsLayer.transform.m11)
     self.delegate?.didRotate(toAngle: a.degrees())
   }

}

extension ClockFace {
  
  private func drawPoints(radius: CGFloat, count: Int) {
    
    let side = 2*radius/CGFloat(2.squareRoot())
    let x = (self.width - side)/2
    let replicatorLayer = CAReplicatorLayer()
    replicatorLayer.frame = CGRect(x: x, y: x, width: side, height: side)
    replicatorLayer.instanceCount = count
    
    let angle = -CGFloat.pi * 2 / CGFloat(count)
    replicatorLayer.instanceTransform = CATransform3DMakeRotation(angle, 0, 0, 1)

    self.layer.addSublayer(replicatorLayer)

    let center = UIBezierPath(arcCenter: .zero,
                              radius: 2,
                              startAngle: CGFloat(0),
                              endAngle: CGFloat(Double.pi * 2),
                              clockwise: true)
    
    let centerLayer = CAShapeLayer()
    centerLayer.path = center.cgPath
    centerLayer.fillColor = Constants.Colors.primary.cgColor
    centerLayer.strokeColor = Constants.Colors.primary.cgColor
    centerLayer.lineWidth = 2.0
    
    // Create layer that will be replicated 12 times to form a complete circle
    replicatorLayer.addSublayer(centerLayer)
  }
  
  
  private func drawNumbers(radius: CGFloat) {
    
    let count = 24
    let txtRadius = radius * 0.95
    
    let paragraph = NSMutableParagraphStyle()
    paragraph.alignment = .center
    
    let angleTick: CGFloat = (CGFloat.pi * 2.0) / CGFloat(count)

    for i in 1...count {
      
      let aFont = UIFont.systemFont(ofSize: i%6 == 0 ? radius/9 : radius/13, weight: .light)
      let attr = [NSAttributedString.Key.font: aFont,
                  NSAttributedString.Key.paragraphStyle: paragraph,
                  NSAttributedString.Key.foregroundColor: UIColor.white]
      
      let angle = angleTick * CGFloat(i + 6)
      let number = NSAttributedString(string: String(i), attributes: attr)
      
      let numberSize = number.size()
      let x = radius + CGFloat(cos(angle)) * txtRadius - numberSize.width/2
      let y = radius + CGFloat(sin(angle)) * txtRadius - numberSize.height/2
      let numberRect = CGRect(x: x, y: y, width: numberSize.width, height: numberSize.height)

      number.draw(in: numberRect)
    }
  }


  private func addHalfCircle(center: CGPoint, radius: CGFloat, startAngle: CGFloat, endAngle: CGFloat, fillColor: UIColor, top: Bool) -> CAShapeLayer {
    
    let shape = CAShapeLayer()
    shape.frame = self.bounds
    shape.fillColor = fillColor.cgColor
    let path = UIBezierPath(circleSegmentCenter: center,
                           radius: radius,
                           startAngle: startAngle + 90,
                           endAngle: endAngle + 90)
    shape.path = path.cgPath
    
    self.layer.insertSublayer(shape, at: 0)
    
    return shape
  }
}
