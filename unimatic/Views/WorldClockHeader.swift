//
//  WorldClockHeader.swift
//  unimatic
//
//  Created by Filippo Tosetto on 31/07/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit
import RealmSwift

protocol WorldClockHeaderDelegate: class {
  func willAddNewCity()
  func willReset()
  func didScrollToPosition(position: Int)
}

class WorldClockHeader: UIView {

  @IBOutlet weak var plusButton: UIButton!
  @IBOutlet weak var scrollView: UIScrollView! {
    didSet {
      self.scrollView.delegate = self
    }
  }
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var cityLabel: UILabel!
  
  private weak var delegate: WorldClockHeaderDelegate?
  private let dateFormatter = DateFormatter()
  private var locations = [RMLocation]()
  private var currentDate = Date()
  private var locationQuery: RMQuery<RMLocation>?


  func configure(inView view: UIView, withDelegate delegate: WorldClockHeaderDelegate?) {
    self.translatesAutoresizingMaskIntoConstraints = false
        
    view.addSubview(self)
    view.addConstraints([
      self.heightAnchor.constraint(equalToConstant: 150),
      self.topAnchor.constraint(equalTo: view.topAnchor),
      self.leftAnchor.constraint(equalTo: view.leftAnchor),
      self.rightAnchor.constraint(equalTo: view.rightAnchor)
    ])
    
    self.delegate = delegate
    self.dateFormatter.dateFormat = "HH:mm"

    self.locationQuery = RMQuery.locationsQuery(delegate: self)
  }
  
  func updateDateLabel(withDate date: Date) {
    self.currentDate = date
    
    self.setupScrollView()
  }
  
  
  func reloadData() {
    self.setupScrollView()
  }
  
  private func setupScrollView() {

    self.scrollView.removeAllSubviews()
    
    for item in self.locations.enumerated() {
      let timeLabel = UILabel()
      self.dateFormatter.timeZone = item.element.timezone
      timeLabel.frame = CGRect(origin: CGPoint(x: self.scrollView.width*CGFloat(item.offset), y: 0),
                               size: self.scrollView.size)
      timeLabel.text = self.dateFormatter.string(from: self.currentDate)
      timeLabel.textColor = UIColor.green
      timeLabel.textAlignment = .center
      timeLabel.font = UIFont.systemFont(ofSize: 66, weight: .ultraLight)
      self.scrollView.addSubview(timeLabel)
    }
    
    self.pageControl.numberOfPages = self.locations.count
    
    self.scrollView.contentSize = CGSize(width: self.scrollView.width*CGFloat(self.locations.count),
                                         height: self.scrollView.height)
    
    self.refreshLabel()
  }
  
  @IBAction func addButtonAction(_ sender: Any) {
    self.delegate?.willAddNewCity()
  }
  
  @IBAction func resetAction(_ sender: Any) {
    self.delegate?.willReset()
  }
}

// MARK: - Private

extension WorldClockHeader {
  
  private func refreshLabel() {
    guard self.locations.count > self.pageControl.currentPage else {
      return
    }
    let location = self.locations[self.pageControl.currentPage]
      
    self.cityLabel.text = String(location.name.split(separator: ",").first ?? "")
  }
}

// MARK: - UIScrollViewDelegate

extension WorldClockHeader: UIScrollViewDelegate {
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let width = self.scrollView.width

    let pageIndex = round(scrollView.contentOffset.x/width)
    self.pageControl.currentPage = Int(pageIndex)
        
    self.refreshLabel()
    
    self.delegate?.didScrollToPosition(position: self.pageControl.currentPage)
  }
}

// MARK: - RMQueryDelegate

extension WorldClockHeader: RMQueryDelegate {
  
  func realmQueryDidChange(_ changes: RMQueryChanges, query: AnyObject) {
    guard let locations = self.locationQuery?.results else {
      return
    }
    
    self.locations = Array(locations)
    self.reloadData()
  }
}
