//
//  HeaderView.swift
//  unimatic
//
//  Created by Filippo Tosetto on 14/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: class {
  func willShowWorldClock()
  func willShowCurrentTime()
  func willShowStopWatch()
}

class HeaderView: UIView {

  @IBOutlet weak var settingsButton: UIButton!
  @IBOutlet weak var infoButton: UIButton!
  
  @IBOutlet weak var titleLabel: UILabel!
  
  @IBOutlet weak var currentTimeLabel: UILabel!
  @IBOutlet weak var worldClockLabel: UILabel!
  @IBOutlet weak var stopWatchLabel: UILabel!
  @IBOutlet weak var worldClockButton: UIButton! {
    didSet {
      self.worldClockButton.setImage(UIImage(named: "world-clock-icon"), for: .normal)
      self.worldClockButton.setImage(UIImage(named: "world-clock-icon-selected"), for: .selected)
      self.worldClockButton.setImage(UIImage(named: "world-clock-icon-selected"), for: .highlighted)
    }
  }
  @IBOutlet weak var stopWatchButton: UIButton! {
    didSet {
      self.stopWatchButton.setImage(UIImage(named: "stop-watch-icon"), for: .normal)
      self.stopWatchButton.setImage(UIImage(named: "stop-watch-icon-selected"), for: .selected)
      self.stopWatchButton.setImage(UIImage(named: "stop-watch-icon-selected"), for: .highlighted)
    }
  }
  @IBOutlet weak var currentTimeButton: UIButton! {
    didSet {
      self.currentTimeButton.setImage(UIImage(named: "current-time-icon"), for: .normal)
      self.currentTimeButton.setImage(UIImage(named: "current-time-icon-selected"), for: .highlighted)
      self.currentTimeButton.setImage(UIImage(named: "current-time-icon-selected"), for: .selected)
    }
  }
  
  private weak var delegate: HeaderViewDelegate?
  
  func configure(withInView view: UIView, delegate: HeaderViewDelegate?) {
    self.translatesAutoresizingMaskIntoConstraints = false
      
    view.addSubview(self)
    view.addConstraints([
      self.heightAnchor.constraint(equalToConstant: 141),
      self.bottomAnchor.constraint(equalTo: view.bottomAnchor),
      self.leftAnchor.constraint(equalTo: view.leftAnchor),
      self.rightAnchor.constraint(equalTo: view.rightAnchor)
    ])
    
    self.delegate = delegate
  }

  func selectCurrentTime() {
    self.currentTimeButton.isSelected = true
    self.currentTimeLabel.textColor = Constants.Colors.primary
    
    self.stopWatchButton.isSelected = false
    self.stopWatchLabel.textColor = Constants.Colors.secondary
    self.worldClockButton.isSelected = false
    self.worldClockLabel.textColor = Constants.Colors.secondary
  }
  
  func selectWorldClock() {
    self.worldClockButton.isSelected = true
    self.worldClockLabel.textColor = Constants.Colors.primary

    self.currentTimeButton.isSelected = false
    self.currentTimeLabel.textColor = Constants.Colors.secondary
    self.stopWatchButton.isSelected = false
    self.stopWatchLabel.textColor = Constants.Colors.secondary
  }
  
  func selectStopWatch() {
    self.stopWatchButton.isSelected = true
    self.stopWatchLabel.textColor = Constants.Colors.primary
    
    self.worldClockButton.isSelected = false
    self.worldClockLabel.textColor = Constants.Colors.secondary
    self.currentTimeButton.isSelected = false
    self.currentTimeLabel.textColor = Constants.Colors.secondary
  }
  
  @IBAction func infoAction(_ sender: Any) {
  }
  
  @IBAction func settingsAction(_ sender: Any) {
  }
  
  @IBAction func currentTimeAction(_ sender: Any) {
    self.delegate?.willShowCurrentTime()
  }
  
  @IBAction func worldClockAction(_ sender: Any) {
    self.delegate?.willShowWorldClock()
  }
  
  @IBAction func stopWatchAction(_ sender: Any) {
    self.delegate?.willShowStopWatch()
  }
  
}
