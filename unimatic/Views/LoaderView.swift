//
//  LoaderView.swift
//  unimatic
//
//  Created by Filippo Tosetto on 04/08/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class LoaderView: UIView {

  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

  func configure(inView view: UIView) {
    self.translatesAutoresizingMaskIntoConstraints = false
        
    view.addSubview(self)
    view.addConstraints([
      self.bottomAnchor.constraint(equalTo: view.bottomAnchor),
      self.topAnchor.constraint(equalTo: view.topAnchor),
      self.leftAnchor.constraint(equalTo: view.leftAnchor),
      self.rightAnchor.constraint(equalTo: view.rightAnchor)
    ])
    
    self.activityIndicator.startAnimating()
  }
  
  func dismiss() {
    UIView.animate(withDuration: 0.3, animations: {
      self.alpha = 0.0
    }) { _ in
      self.removeFromSuperview()
    }
  }
}
